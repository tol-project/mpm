//////////////////////////////////////////////////////////////////////////////
Class @RayInPolytope
//////////////////////////////////////////////////////////////////////////////
{
  Real _.isCartesian;
  Real _.h.min;
  Real _.h.max,
  Real _.h.length;
  VMatrix _.x;
  VMatrix _.u;
  
  ////////////////////////////////////////////////////////////////////////////
  Static @RayInPolytope Create(
    VMatrix A, VMatrix a, VMatrix x0, VMatrix v)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix u = v * 1/Sqrt(VMatSum(v^2));
    VMatrix b = a - A*x0;
    VMatrix v = A*u;
    Set v.pos = MatQuery::SelectRowsWithValue(GT(v,0),1);
    Set v.neg = MatQuery::SelectRowsWithValue(LT(v,0),1);
    
    Real h.max =  VMatMin( SubRow(b,v.pos) $/ SubRow(v,v.pos));
    Real h.min = -VMatMin(-SubRow(b,v.neg) $/ SubRow(v,v.neg));
    
    Real h = h.max-h.min;
    @RayInPolytope ray = [[
      Real _.isCartesian = ?,
      Real _.h.min = h.min,
      Real _.h.max = h.max,
      Real _.h.length = h,
      VMatrix _.x = Copy(x0);
      VMatrix _.u = Copy(u)
    ]]
  };
  

  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix RandDirection(Real n, Real cartesian)
  ////////////////////////////////////////////////////////////////////////////
  {    
    VMatrix u = If(!cartesian,
    {
      VMatrix v = Gaussian(n,1,0,1);
      v * 1/Sqrt(VMatSum(v^2))
    },
    {
      Real j = IntRand(1,n);
      Convert(Triplet(Row(j,1,1),n,1),"Cholmod.R.Sparse")
    })
  };

  
  ////////////////////////////////////////////////////////////////////////////
  Static @RayInPolytope RandRayTry(
    VMatrix A, VMatrix a, VMatrix x0, Real cartesian, Real try)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix u = @RayInPolytope::RandDirection(VRows(x0),cartesian);
    NameBlock ray = @RayInPolytope::Create(A,a,x0,u);
    Real ray::_.isCartesian := cartesian;
    Real ok = If(IsUnknown(ray::_.h.length),False,ray::_.h.length>0);
    If(ok & try<=0, ray, @RayInPolytope::RandRayTry(A,a,x0,cartesian,try-1))
  };  
  
  ////////////////////////////////////////////////////////////////////////////
  Static @RayInPolytope RandRay(
    VMatrix A, VMatrix a, VMatrix x0, Real cartesian)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix u = @RayInPolytope::RandDirection(VRows(x0),cartesian);
    NameBlock ray = @RayInPolytope::Create(A,a,x0,u);
    Real ray::_.isCartesian := cartesian;
    ray
  };  

  ////////////////////////////////////////////////////////////////////////////
  Real step.relative(Real r)
  ////////////////////////////////////////////////////////////////////////////
  {
    _.h.min+r*_.h.length
  };

  ////////////////////////////////////////////////////////////////////////////
  Real step.uniform(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    step.relative(Rand(0,1))
  };

  ////////////////////////////////////////////////////////////////////////////
  Real step.truncNormal(Real factorSigma)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real s = _.h.length * factorSigma;
    DrawTruncatedNormal(0,s,_.h.min,_.h.max)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real dens.truncNormal(Real factorSigma, Real h)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real s = _.h.length * factorSigma;
    DensTruncatedNormal(h,0,s,_.h.min,_.h.max)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix move(Real h)
  ////////////////////////////////////////////////////////////////////////////
  {
    _.x+_.u*h
  };
  
  ////////////////////////////////////////////////////////////////////////////
  VMatrix move.uniform(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    move(step.uniform(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix move.truncNormal(Real factorSigma)
  ////////////////////////////////////////////////////////////////////////////
  {
    move(step.truncNormal(factorSigma))
  };

  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix FindInteriorPointOfPolytope(
    VMatrix A, VMatrix a_, Real inDst)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix a = a_-Abs(a_)*inDst-Not(a_)*inDst;
    Real n = VColumns(A);
    Real K = 1;
    VMatrix x = SetSum(For(1,K,VMatrix(Real k)
    {
    TolIpopt::@SQP interiorPoint = { TolIpopt::@SQP::Optimize.Constrained(
      Set options =             
      [[ TolIpopt::@Option("Integer", "print_level", 0) ]],
      VMatrix C_ = Eye(n),
      VMatrix c_ = Rand(n,1,0,0),
      Real c0_ = Rand(0,0), 
      VMatrix Constant(0,0,?),
      VMatrix Constant(0,0,?),
      VMatrix Constant(0,0,?),
      VMatrix A, 
      VMatrix a) };
      interiorPoint::_.x
    })) * 1/K;
    x
  };

  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix FindInteriorPointOfPolytopeCloseToPoint(
    VMatrix x0,
    VMatrix A, VMatrix a_, Real inDst)
  ////////////////////////////////////////////////////////////////////////////
  {
//  VMatrix a = a_-Abs(a_)*inDst-Not(a_)*inDst;
    VMatrix a = a;
    Real n = VColumns(A);
    TolIpopt::@SQP interiorPoint = { TolIpopt::@SQP::Optimize.Constrained(
      Set options =             
      [[ TolIpopt::@Option("Integer", "print_level", 0) ]],
      VMatrix C_ = Eye(n),
      VMatrix c_ = -x0*2,
      Real c0_ = VMatSum(x0^2), 
      VMatrix Constant(0,0,?),
      VMatrix Constant(0,0,?),
      VMatrix Constant(0,0,?),
      VMatrix A, 
      VMatrix a) };
      interiorPoint::_.x
  } 
};
