[[PageOutline]]
= Simulaci�n bayesiana con ''Metropolis within Gibbs'' =

== Introducci�n ==

Supongamos que queremos simular una variable aleatoria vectorial [[LatexEquation( \Omega\in\mathbb{R}^{n} )]] 
cuya funci�n de log-verosimilitud salvo constante es

[[LatexEquation( \ln\pi\left(\Omega\right) )]]

Si es posible evaluar esta funci�n de forma eficiente el m�todo Metropolis-Hastings o cualqueira de sus 
derivados nos permite simular una cadena de Markov de la distribuci�n de [[LatexEquation( \Omega )]].

Si es demasiado compleja como para expresarse de forma anal�tica o bien es demasiado pesada de calcular
entonces hay que tratar de dividir esta variable en bloques

[[LatexEquation( \Omega=\left(\begin{array}{ccc}\Omega_{1} & \cdots & \Omega_{B}\end{array}\right)\quad\wedge\;\Omega_{i}\in\mathbb{R}^{n_{i}}\quad\wedge\;\underset{i=1}{\overset{B}{\sum}}n_{i}=n )]]

Si se conoce un m�todo eficaz para simular cada bloque condicionado al resto entonces es aplicable 
el m�todo de simulaci�n de Gibbs por bloques. 

Normalmente resulta mucho m�s sencillo evaluar la funci�n de log-verosimilitud salvo constante condicionada 
al resto de bloques, 

[[LatexEquation( \ln\pi_{i}\left(\left.\Omega_{i}\right|\Omega_{j\neq i}\right) )]]

lo cual permite aplicar el m�todo Metropolis Within Gibbs, que consiste en simular 
alternativamente los distintos bloques con el m�todo de Metropolis-Hastigs (MH) o alguno similar como el 
Multiple Try Metroplis (MTM), dejando invariantes todos aquellos de los que depende. En cada simulaci�n 
es necesario evaluar la log-verosimilitud un n�mero de veces dependiente del m�todo usado.

Este esquema tiene la ventaja de que sirve para simular cualquier clase de modelos implementando bloques de 
simulaci�n para cada tipo de elemento que intervenga y definiendo aparte las relaciones de condicionamiento 
como meras reglas de asignaci�n total o parcial de miembros de un bloque condicionado en funci�n de los 
miembros del bloque condicionante. 

Si para un bloque concreto se dispone de un m�todo de generaci�n condicionada que resulte m�s sencillo o 
eficaz que el MH o sus derivados, es perfectamente l�cito aplicarlo siempre y cuando se respeten las 
relaciones de condicionamiento. 

== Arquitectura del sistema ==

Definiremos modelo de forma abstracta como un conjunto de bloques de variables aleatorias relacionados 
entre s� por mecanismos de condicionamiento que se ejecutan tras cada simulaci�n de uno de ellos, para
forzar as� que la funci�n de log-verosimilitud devuelva siempre los valores condicionados al resto de
bloques.

El esquema planteado supondr�a la existencia de un objeto '''master''' que tuviera un listado de 
objetos '''bloque''' del modelo, que se ir�an a�adiendo con mecanismos de ensamblaje que indicar�an 
c�mo un bloque condiciona a los ya existentes y c�mo es condicionado por ellos seg�n corresponda. 

En lugar de tratar de programar un mini-lenguaje ad-hoc como se hizo en BSR, la idea es que se dote al 
sistema de una familia de clases en TOL que permitan crear declarativamente bloques primarios que implementen 
las distribuciones b�sicas m�s usuales, y de otra familia que permita relacionar bloques entre s� de forma 
algebraica para que queden perfectamente definidos los condicionamientos correspondientes.

=== Paralelizaci�n de procesos ===

Cuando un modelo es muy complejo, tiene demasiados bloques, o �stos tienen muchas variables, o de una 
u otra forma involucran el manejo de grandes cantidades de informaci�n, los modelos se hacen inmanejables
si se pretende hacerlo en una computadora individual. Puede ser que no haya memoria suficiente o que a�n 
habi�ndola la velocidad de c�lculo sea muy inferior a la requerida, con lo que es necesario paralelizar 
los procesos en diferentes m�quinas para acomoter el problema.

[[Image(source:tolp/OfficialTolArchiveNetwork/MWG/doc/esquema.02.png, align=right)]]

Merece la pena puntualizar que se puede simular simult�neamente en paralelo aquellos bloques que no 
dependen entre s�, pero los que son interdependientes deben simularse secuencialmente para que las  
reglas de condicionamiento sean v�lidas. Para ello el m�ster del simulador debe conocer qu� bloques 
condicionan a qu� otros para establecer un grafo topol�gico de la red que forman y determinar en qu� 
partes es posible paralelizar. No necesita saber c�mo se relacionan los bloques entre s� ni c�mo se
implementan los condicionamientos, sino s�lo entre qu� bloques se dan y en qu� sentido o sentidos, 
pues el condicionamiento puede ser unidireccional o bidireccional. 

En la imagen de la derecha se presenta un caso muy simple pero bastante habitual de modelo en el que 
se observan
 * una serie de bloques [[LatexEquation(B_1)]] a [[LatexEquation(B_{n})]] est�n relacionados entre 
   s� por lo que deben ser ejecutados secuencialmente.
 * otro conjunto de bloques [[LatexEquation(B_{n+1})]] a [[LatexEquation(B_{n+k})]] independientes 
   entre s�, que pueden por tanto ejecutarse en paralelo en distintas m�quinas.
 * una �ltima serie de bloques [[LatexEquation(B_{n+k+1})]] a [[LatexEquation(B_{m})]] dependientes 
   entre s�, que deben ejecutarse secuencialmente.

Tras la simulaci�n del �ltimo bloque se debe recolectar el estado actual de cada bloque como un vector 
fila que se concatena en el proceso maestro a los dem�s en el orden establecido, y despu�s se vuelve 
al primero de los bloques para comenzar una nueva simulaci�n, y as� durante miles o hasta millones de veces. 

Por este motivo es necesario que la transmisi�n de informaci�n que implica cada condicionamiento se 
realice de la forma m�s eficiente que sea posible, lo cual requiere un protocolo de comunicaci�n adecuado 
cuando se establece un condicionamiento entre bloques manejados por distintas m�quinas, como ocurre por 
ejemplo en los condicionamientos entre los bloques [[LatexEquation(B_{n})]] y [[LatexEquation(B_{n+1})]] 
o entre [[LatexEquation(B_{n+k})]] y [[LatexEquation(B_{n+k-1})]] del anterior diagrama.

=== Entidades b�sicas ===

Existen muchas formas de relaci�n entre bloques. La acci�n a llevar a cabo para condicionar 
puede suponer desde modificar simplemente un valor escalar (como la sigma de un bloque de regresi�n 
lineal normal), hasta modificar total o parcialmente un valor vectorial o matricial, (como ocurre en 
el caso de los omitidos que deben trasladarse a un subconjunto arbitrariamente salteado de entre las 
celdas de la matriz de input o de output). Adem�s, los valores con los que hay que sustituir los 
valores a condicionar no tienen porqu� ser directamente los valores condicionadores, sino que en 
general ser�n el resultado de una funci�n dependiente de los mismos. Por estos motivos no resulta ni 
mucho menos trivial condicionar de una forma gen�rica y eficiente. 

Existen varias entidades en el sistema, algunas de las cuales dar�n lugar a una jerarqu�a de clases 
que permitir�n adaptarse a todas las situaciones con una API m�nima, mientras que otros conceptos son 
relativos al uso y no a su estructura por lo que no precisan de una clase

 * {{{@RandVar}}}: Variable aleatoria generable mediante log-verosimilitud, sin perjuicio de que pueda haber
   otro m�todo de generaci�n alternativo m�s eficaz en alguna clase heredada. 
 * {{{@Relation}}}: Relaci�n entre un bloque condicionante y otro condicionado que especifica sin ambig�edad 
   posible todas las acciones a realizar.
 * {{{@Model}}}: Conjunto de bloques de variables aleatorias y de relaciones de condicioanmiento. Es un 
   tipo especial de variale aleatoria, por lo que hereda de {{{@RandVar}}}.
 * Bloque: Variable aleatoria cuyo generador es llamado por un modelo seg�n una estrategia de ejecuci�n. 
   de alg�n modo compatible con el algoritmo de Gibbs. Un modelo puede funcionar como un bloque de otro modelo.
 * Estrategia: Forma de alternar o simultanear la simulaci�n de los distintos bloques de Gibbs, de forma 
   compatible con la estructura de relaciones. 
 * Master: Modelo principal formado por bloques que pueden ser a su vez modelos y que lanza las simulaciones 
   de cada uno de ellos seg�n la estrategia seleccionada y la va almacenando en disco. 

== Clase {{{@RandVar}}} ==
A la clase base de la que derivan todos las variables aleatorias (v.a.) la llamaremos {{{Class @RandVar}}}, pues 
lo que se est� planteando no es otra cosa que un mecanismo de gesti�n de variables aleatorias por la 
v�a de la generaci�n de simulaciones por uno u otro m�todo. 

No toda v.a. definida tiene porqu� ser un bloque de Gibbs de un modelo. Muchas veces un bloque 
se puede definir mediante operaciones algebraicas de otras variables aleatorias las cuales no pertenecer�n 
directamente al modelo sino indirectamente a trav�s de uno de sus bloques. Otro tipo de v.a. de gran
utilidad ser�n las que definan la informaci�n ''a priori'' relativa a un bloque o a un modelo, simplemente
sumando su log-verosimilitud.

En este nivel ra�z estar� disponible una bater�a de algoritmos de generaci�n MCMC basados en la
log-verosimilitud exclusivamente y que sean capaces de autocalibrarse sin intervenci�n del usuario, pues 
deben adecuarse a cualquier distribuci�n que se quiera generar:
{{{
#!java  
  //Generates a single sample by means of Metropolis-Hastings algorithm
  VMatrix draw.MH(Real numSim) { ... }
  //Generates a single sample by means of Multiple Try Metropolis algorithm
  VMatrix draw.MTM(Real numSim) { ... }
  ...
}}}
El argumento {{{numSim}}} indica el n�mero de simulaci�n actual y es meramente informativo, para
generaci�n de errores y para posibles actuaciones dependientes de ese valor, por ejemplo durante 
el periodo de burning. 
{{{
#!java  
  //Generates a single sample
  VMatrix draw(Real numSim) { draw.MH(numSim) }
}}}

El m�todo por defecto ser� Metropolis-Hastings y podr� ser cambiado en las clases heredadas para las 
que exista otro m�todo directo m�s eficaz de muestreo.

Cuando no existe dicho m�todo directo la clase derivada final, �sta debe implementar obligatoriamente 
el m�todo que devuelve el logaritmo de la verosimilitud para poder aplicar alguno de los m�todos MCMC
{{{
#!java  
  //Returns log-likelihood except a constant for a given a vector as defined by inherited class
  Real _log_likelihood(VMatrix x);
}}}
Se trata de un m�todo privado porque en la clase ra�z se implementar� el m�todo p�blico
{{{
#!java  
  //Returns log-likelihood except a constant for a given a vector
  Real log_likelihood(VMatrix x) { ... }
}}}
que har� las comprobaciones b�sicas pertinentes, llamar� al m�todo privado y efectuar� otro tipo de 
tareas internas que se ir�n explicando m�s adelante.

El m�todo toma siempre un vector definido como un objeto de tipo VMatrix aunque se espere un escalar,
para simplificar la API. Las dimensiones del argumento tienen que ser compatibles con lo que espera el 
bloque y de lo contrario se mostrar� un error y se devolver� omitido. Si se quiere pasar un escalar y 
tiene sentido hacerlo se puede llamar al m�todo
{{{
#!java  
  //Returns log-likelihood except a constant for a given a sample
  Real log_likelihood.S(Real x) { log_likelihood(Constant(1,1,x) };
}}}
Si el argumento est� fuera del dominio la verosimilitud es cero y su logaritmo es [[LatexEquation(-\infty)]].
Todas las restricciones de igualdad o desigualdad, lineales o no, estar�n reflejadas en el hecho de que se 
devolver� [[LatexEquation(-\infty)]] cuando no se cumplan.

=== Variables aleatorias primarias ===

A los bloques primarios los llamaremos Primary Random Variable (PRV) y permiten tratar con variables 
aleatorias correspondientes a las distribuciones b�sicas que se requieran:

 * Distribuci�n determinista: {{{@PRV.Cte::Create.V(VMatrix cte) }}}
 * Distribuci�n uniforme continua: {{{@PRV.UniCon::Create(Real size, Real from, Real until) }}}
 * Distribuci�n uniforme discreta: {{{@PRV.UniDis::Create(Real size, Real from, Real until) }}}
 * Distribuci�n normal: {{{@PRV.Normal::Create(Real size, Real nu, Real sigma) }}}
 * Distribuci�n log�stica: {{{@PRV.Logistic::Create(Real size, ) }}}
 * Distribuci�n de Bernouilli: {{{@PRV.Bernouilli::Create(Real size, Real p) }}}
 * Distribuci�n binomial: {{{@PRV.Binomial::Create(Real size, Real p, Real k) }}}
 * Distribuci�n de Poisson: {{{@PRV.Poisson::Create(Real size, Real nu) }}}
 * Distribuci�n t-student: {{{@PRV.TStudent::Create(Real size, Real freDeg) }}}
 * Distribuci�n chi-square: {{{@PRV.ChiSquare::Create(Real size, Real freDeg) }}}
 * Distribuci�n scaled inverse chi-square: {{{@PRV.InvScaChiSquare::Create(Real size, Real freDeg, Real scale) }}}
 * ...

En todas aquellas para las que sea posible, que ser�n pr�cticamente todas, se dispondr� de un m�todo 
especializado de generaci�n directa de muestras sobrecargando el m�todo {{{draw}}}

=== Variables aleatorias derivadas algebraicamente ===

A los bloques derivados algebraicamente los llamaremos Algebraic Derived Random Variable (RVOS) y 
permiten generar variables aleatorias que pueden ser muy complicadas de generar directamente, pero 
en cambio es trivial generarlas aplicando las operaciones sobre las generaciones de los argumentos.

==== Operaciones escalares ====

En este tipo de clases heredadas de {{{@RandVar}}} se puede aplicar operaciones aritm�ticas sobre instancias de 
{{{@RandVar}}} previamente creadas o bien sobre dichas instancias y valores escalares deterministas

 * ''Suma'': 
  * {{{@RVOS.Sum::Create(@RandVar a, @RandVar b)}}}, 
  * {{{@RVOS.Sum::Create.RV.S(@RandVar a, Real b)}}}, 
  * {{{@RVOS.Sum::Create.S.RV(Real a, @RandVar b)}}}
 * ''Resta'': 
  * {{{@RVOS.Dif::Create(@RandVar a, @RandVar b)}}},
  * {{{@RVOS.Dif::Create.RV.S(@RandVar a, Real b)}}}, 
  * {{{@RVOS.Dif::Create.S.RV(Real a, @RandVar b)}}}
 * ''Producto'':
  * {{{@RVOS.Prod::Create(@RandVar a, @RandVar b)}}}, 
  * {{{@RVOS.Prod::Create.RV.S(@RandVar a, Real b)}}}, 
  * {{{@RVOS.Prod::Create.S.RV(Real a, @RandVar b)}}}
 * ''Cociente'':
  * {{{@RVOS.Div::Create(@RandVar a, @RandVar b)}}},
  * {{{@RVOS.Div::Create.RV.S(@RandVar a, Real b)}}}, 
  * {{{@RVOS.Div::Create.S.RV(Real a, @RandVar b)}}}
 * ''Potencia'':
  * {{{@RVOS.Power::Create(@RandVar a, @RandVar b)}}},
  * {{{@RVOS.Power::Create.RV.S(@RandVar a, Real b)}}}, 
  * {{{@RVOS.Power::Create.S.RV(Real a, @RandVar b)}}}
 * ''Transformaci�n monaria'': Aplica una funci�n de R en R
  * {{{@RVOS.Monary::Create(Code f, @RandVar a)}}}
 * ''Transformaci�n binaria'': Aplica una funci�n de R^2 en R
  * {{{@RVOS.Binary::Create(Code f, @RandVar a, @RandVar b)}}}
  * {{{@RVOS.Binary::Create.RV.S(Code f, @RandVar a, Real b)}}}, 
  * {{{@RVOS.Binary::Create.S.RV(Code f, Real a, @RandVar b)}}}
 * ''Transformaci�n n-aria'': Aplica una funci�n de R^n en R
  * {{{@RVOS.N_ary::Create(Code f, Set a)}}}

En este tipo de operaciones resulta trivial simular llamando al simulador de las variables aleatorias 
involucradas y aplicando la operaci�n sobre ellas. En cambio, si hay m�s de una variable aleatoria
incvolucrada, el c�lculo de la verosimilitud conlleva una integral num�rica de alto coste computacional, 
por lo que el m�todo devolver� omitido y no debe usarse.
{{{
#!java
Real _log_likelihood(VMatrix x) { ? }
}}}
Si se trata de una operaci�n monaria o todos los argumentos son deterministas salvo uno entonces no hay 
ning�n problema en calcular la verosimilitud aplicando primero la transformaci�n inversa y llamando al
m�todo de la �nica v.a. propiamente dicha.

==== Operaciones matriciales ====

En este tipo de clases heredadas de {{{@RandVar}}} se puede aplicar operaciones aritm�ticas sobre instancias de 
{{{@RandVar}}} previamente creadas o bien sobre dichas instancias y valores vectoriales o matriciales deterministas

 * ''Extracci�n'': 
  * Por �ndice: {{{@RVOV.Extract.Cell::Create(@RandVar a, Set ij)}}}
  * Por rango: {{{@RVOV.Extract.Range::Create(@RandVar a, Real ini, Real num)}}}
 * ''Suma'':
  * Post-Matricial: {{{@RVOV.Sum.M.Post::Create(@RandVar a, VMatrix b)}}},
  * Pre-Matricial: {{{@RVOV.Sum.M.Pre::Create(VMatrix a, @RandVar b)}}}
 * ''Producto'':
  * Post-Matricial: {{{@RVOV.Prod.M.Post::Create(@RandVar a, VMatrix b)}}},
  * Pre-Matricial: {{{@RVOV.Prod.M.Pre::Create(VMatrix a, @RandVar b)}}},
  * Celda a celda: {{{@RVOV.Prod.M.Weighted::Create(@RandVar a, VMatrix b)}}}
 * ''Cholesky'': 
  * {{{@RVOV.Chol.Prod::Create(VMatrix X, Text mode, @RandVar a)}}}: Aplica el factor de Cholesky de la matriz 
    dada a una v.a. El argumento mode puede ser "X","XtX" � "XXt" seg�n se disponga de la matriz sim�trica o de 
    un factor suyo.
  * {{{@RVOV.Chol.Solve.L::Create(VMatrix X, Text mode, @RandVar a)}}}: Resuelve el sistema {{{ L*b=a }}} siendo L el 
    factor de Cholesky de la matriz dada. El argumento  mode puede ser "X","XtX" � "XXt" seg�n se disponga de la matriz 
    sim�trica o de un factor suyo.
  * {{{@RVOV.Chol.Solve.Lt::Create(VMatrix X, Text mode, @RandVar a)}}}: Resuelve el sistema {{{ L'*b=a }}} siendo L el 
    factor de Cholesky de la matriz dada. El argumento  mode puede ser "X","XtX" � "XXt" seg�n se disponga de la matriz 
    sim�trica o de un factor suyo.
  * {{{@RVOV.Chol.Solve.LtL::Create(VMatrix X, Text mode, @RandVar a)}}}: Resuelve el sistema {{{ L'*L*b=a }}} siendo L el 
    factor de Cholesky de la matriz dada. El argumento  mode puede ser "X","XtX" � "XXt" seg�n se disponga de la matriz 
    sim�trica o de un factor suyo.
  * {{{@RVOV.Chol.Solve.LLt::Create(VMatrix X, Text mode, @RandVar a)}}}: Resuelve el sistema {{{ L*L'*b=a }}} siendo L el 
    factor de Cholesky de la matriz dada. El argumento  mode puede ser "X","XtX" � "XXt" seg�n se disponga de la matriz 
    sim�trica o de un factor suyo.
 * ''Regresor lineal'': {{{Y=X*b+e}}} 
  * {{{@RVOV.LinReg::Create(VMatrix Y, VMatrix X, @RandVar e)}}}: Llama al log_likelihood de e con el resultado de Y-X*b
  * {{{@RVOV.LinReg.Chol::Create(VMatrix Y, VMatrix X, @RandVar e)}}} : Calcula directamente {{{b = (X'X)^-1 X' (Y-e)}}} 
    Es �til cuando X es regular y constante y los residuos son normales de media 0, independientes y homoced�sticos 
    pues resulta muy eficaz en esos casos, especialmente si X es adem�s sparse. Internamente se utiliza la descomposici�n 
    de Cholesky.

== Clase {{{@Relation}}} ==

Como ya se ha dicho la clase {{{@Relation}}} implementa una relaci�n de condicionamiento entre un bloque de 
origen o condicionador y otro de destino o condicionado. 

=== Relaciones de condicionamiento por asignaci�n ===

En estos casos, cada bloque tiene su propia funci�n de log-verosimilitud en la que alguno de sus 
elementos es una variable del otro, por lo que el condicionamiento se limita a que un bloque 
modifica en el otro lo que toque.

=== Relaciones de condicionamiento parasitario ===

Existe una amplia gama de filtros no lineales que modifican elementos como el output o el input de 
una regresi�n lineal. En todos ellos el bloque no lineal se limita a cambiar lo que toque 
del bloque de regresi�n, y su funci�n de log-verosimilitud puede implementarse de forma parasitaria
respecto a la de su anfitri�n, llam�ndola cambiando no sus argumentos sino los miembros adecuados
de la instancia.

Este tipo de relaci�n entre bloques es extensible formalmente a todas las situaciones en las que
hay un bloque anfitri�n y otro par�sito, cuya funci�n de log-verosimilitud consistir�a en modificar 
lo que toque dentro del bloque anfitri�n y llamar a su funci�n de log-verosimilitud 
sin modificar los argumentos. De esta forma el bloque anfitri�n no tiene que realizar 
ninguna acci�n de condicionamiento espec�fica sobre el bloque par�sito, pues es inherente a la 
forma de c�lculo. En este tipo de relaci�n, las acciones necesarias para el condicionamiento y para
la evaluaci�n de la log-verosimilitud son exactamente las mismas.

En la clase ra�z {{{@RandVar}}} se dispondr� de un miembro
{{{
#!java
  Set _.hosts = Copy(Empty);
}}}
para que los bloques parasitarios referencien a sus anfitriones 

== Clase {{{@Model}}} ==

Como ya se ha dicho un modelo es un conjunto de bloques y de relaciones de condicionamiento y es un caso
paricular de variable aleatoria. Por lo tanto, un modelo puede ser un bloque de otro modelo y tener 
relaciones de condicionamiento con otros bloques que a su vez podr�an ser modelos o bloques simples.

Llamaremos '''master''' al modelo ra�z que no act�a como bloque de ning�n otro y es el encargado de 
manejar todo el sisteam de simulaci�n.

Un modelo se declara siempre vac�o en primera instancia y despu�s se le van a�adiendo sus componentes
de forma coherente, es decir, para poder a�adir una relaci�n entre dos bloques, estos deben haber sido
incluidos previamente en el modelo.

No todas las variables aleatorias empleadas en la definci�n de un modelo son de inter�s para el analista 
y a veces puede ser muy pesado almacenarlas f�sicamente en el disco, por lo que no hay necesidad de 
hacerlo, pues en una cadena de Markov s�lo es necesario conocer el �ltimo valor generado. Por este motivo
el m�todo con el que se a�ade una variable aleatoria incluye otro argumento que indique si se quiere
o no almcenarla en la MCMC.

{{{
  //Adds a random variable to the model. If argument <storedInMcmc> is true it will be stored at Markov
  //chain matrix into a disk binary file.
  Real model::AddRandVar(@RandVar a, Real storedInMcmc);
}}}

El m�todo para a�adir una relaci�n ser� el siguiente
{{{
#!java
  //Adds a conditioning relation
  Real model::AddRelation(@Relation r);
}}}

== Casos particulares y ejemplos ==

A continuaci�n se describen conceptualmente los elementos utilizados habitualmente en modelos de 
regresi�n bayesiana, clasificados funcionalmente seg�n la metodolog�a a emplear en cada uno.

=== La varianza de una regresi�n lineal normal ===

El bloque de varianzas, con distribuci�n ''chi-cuadrado inversa'' condiciona al bloque normal 
modificando directamente el valor de la varianza mientras que el bloque lineal condiciona al de varianzas
modificando el par�metro de escala poniendo el valor de la suma de cuadrados de los residuos 
homoced�sticos. Este ser�a un caso que podr�amos llamar condicionamiento sim�trico pues cada uno 
condiciona al otro y suele aparecer cuando se aplica un 
[http://en.wikipedia.org/wiki/Conjugate_prior prior conjugado].

=== Datos omitidos en una regresi�n lineal ===

Cuando hay omitidos la regresi�n es en realidad bilineal y es complicado simular conjuntamente 
pero es muy sencillo hacerlo condicionalmente en dos bloques, el principal (anfitri�n) y el 
de omitidos (par�sito). 

Obs�rvese que el bloque anfitri�n no tendr�a porqu� ser una regresi�n, sino que podr�a ser 
cualquier tipo de bloque que contenga una matriz de datos. El bloque de omitidos s�lo tiene 
que tener la referencia del bloque principal, un identificador del miembro a modificar, y 
una lista con ubicaci�n de los omitidos dentro del mismo.

Este esquema es ampliable al caso en el que varios bloques compartan uno o m�s omitidos,
siempre y cuando se trate de bloques independientes entre s�. Simplemente habr�a que sumar las 
log-verosimilitudes de cada uno de ellos. Esta situaci�n sucede con naturalidad en modelos 
jer�rquicos en los que distintos nodos comparten un input y en modelos encadenados en los 
que el output de un nodo es input de otro.

=== Efectos aditivos en t�rminos originales ===

Es muy habitual que haya que aplicar cierta transformaci�n instant�nea al output de una regresi�n lineal 
para conseguir residuos homoced�sticos. Pero luego puede ocurrir que existan efectos que ocurren en 
t�rminos originales y otros en t�rminos transformados 

[[LatexEquation( T\left(Y - X_0 \beta_0 \right) = X_1 \beta_1 + e )]]

Lo �nico que tiene que hacer el filtro par�sito es modificar el output completo del bloque anfitri�n.

=== Ecuaci�n en diferencias ===

En modelos de series temporales, tanto en el caso de la funci�n de transferencia con deltas como el 
caso ARIMA hay que resolver una ecuaci�n en diferencias 

[[LatexEquation( P\left(B\right) u_t = Q\left(B\right) v_t )]]

por lo que ser�a l�gico plantear un bloque gen�rico que diera servicio a ambos casos. 

Los par�metros de este bloque se clasifican en:
 * [[LatexEquation( P\left(B\right) )]] : coeficientes del numerador 
 * [[LatexEquation( Q\left(B\right) )]] : coeficientes del denominador
 * [[LatexEquation( u_0 )]] : valores iniciales del numerador
 * [[LatexEquation( v_0 )]] : valores iniciales del denominador

Hay que tener en cuenta que ambos polinomios podr�an venir factorizados en varias estacionalidades, 
pero al fin y al cabo todo se reduce a coeficientes y a una estructura que rellenar con ellos y que 
contendr�a tambi�n las diferencias si las hubiere.

El denominador ha de ser siempre estacionario, pero el numerador de una funci�n de transferencia 
puede ser completamente arbitrario mientras que el de un ARIMA debe ser estacionario siempre.

En el caso ARIMA caso el bloque de regresi�n ser�a el bloque parasitario y tendr�a que modificar la
serie de ruido ARIMA (denominador) tomada de la regresi�n para construir los residuos (denominador).
El bloque ARIMA ser�a a su vez parasitario de un bloque de resiudos normales.

En cambio, en el caso de funci�n de transferencia, el bloque parasitario ser�a el de la ecuaci�n en 
diferencias que tendr�a que modificar una columna de la matriz de inputs del bloque de regresi�n 
utilizando la serie del denominador, mientras que el numerador ser�a el input original.

=== Relaciones jer�rquicas lineales ===

Sea una relaci�n jer�rquica lineal extendida

[[LatexEquation( H \beta \sim N\left(G \alpha, \Sigma\right) )]]

en la que [[LatexEquation( \alpha )]] son las variables latentes del nivel superior y 
[[LatexEquation( \beta)]] las de nivel inferior definidas en uno o varios bloques, y que pueden ser a 
su vez latentes o primarias.

El bloque de nivel inferior condiciona a todos sus superiores modificando valor actual de las 
[[LatexEquation( \beta)]]. De esta forma nos quedar�a en el superior una regresi�n lineal en la que 
el output ser�a [[LatexEquation( H \beta )]], es decir, que ir�a cambiando en cada iteraci�n.

La funci�n de log-verosimilitud del nivel inferior debe llamar aditivamente a todas las de los 
de nivel superior en las que interviene y sumarse a su propia log-verosimilitud.

=== Informaci�n a priori ===

En el caso de haber informaci�n a priori sobre los par�metros de un bloque cualquiera habr�a que sumar 
la log-verosimilitud correspondiente a su distribuci�n. Lo ideal es usar un
[http://en.wikipedia.org/wiki/Conjugate_prior prior conjugado] siempre que sea posible.

Habr�a que estudiar la posibilidad de utilizar esta informaci�n en la generaci�n de candidatos para 
evitar atascos en la cadena de Markov. 

==== Prior normal ====

Es el m�s habitual de los prior pues es aplicable en muchos ambientes pues la distribuci�n normal es 
autoconjugada y el grueso de los par�metros de un modelo suele ser normal. Cuando no se conoce la 
forma anal�tica de la distribuci�n o es complicado deducir el prior conjugado, pero se puede considerar
razonable que ha de ser sim�trica, entonces el prior normal es sin duda la mejor opci�n.

==== Prior inverse scaled chi-square ====

Esta es la opci�n para indicar el conocimiento adquirido sobre la varianza de una distribuci�n normal.

==== Restricciones deterministas ====

Las restricciones deterministas se implementan como una uniforme en la regi�n factible. Para evitar 
problemas num�ricos se utiliza el [http://en.wikipedia.org/wiki/Penalty_method m�todo de las penalizaciones]
de forma an�loga a su empleo en las t�cnicas de optimizaci�n. 

La log-verosimilitud en la regi�n factible es cero pero fuera de ella en lugar de ser 
[[LatexEquation(-\infty)]] ser� un n�mero negativo proporcional a la distancia a la frontera, que ir� 
creciendo durante la etapa inicial de '''burning''' hasta  alcanzar un valor de penalizaci�n suficientemente 
grande como para que a partir de ese momento resulte altamente improbable que se genere una muestra 
no factible.

No debe usarse una restricci�n determinista si no hay un motivo que lo justifique plenamente. Por ejemplo,
si un coeficiente del modelo es una probabilidad est� claro que debe estar entre 0 y 1, si es una temperatura
en grados kelvin o unaa velocidad entonces debe ser mayor o igual que 0, etc. Si lo que se quiere expresar 
es algo subjetivo o impreciso, como que que es muy improbable que una variable este fuera de un rango, 
entonces se debe utilizar un prior aleatorio, como una normal con una media y una desviaci�n que se adapte 
a esa intuici�n.

{{{
#!java
////////////////////////////////////////////////////////////////////////////////
Class @Constraint : @RandomVar
////////////////////////////////////////////////////////////////////////////////
{
  Static Real _.penalty.initial = 1;
  Static Real _.penalty.factor = 1.1;
  Real _.penalty = ?;

  //////////////////////////////////////////////////////////////////////////////
  Real _penalty_update(Real numSim)
  //////////////////////////////////////////////////////////////////////////////
  {
    Real _.penalty := If(numSim==1, 
      _.penalty.initial, 
      _.penalty.factor * _.penalty) 
  };

  //////////////////////////////////////////////////////////////////////////////
  Real _penalty_eval(VMatrix g)
  //////////////////////////////////////////////////////////////////////////////
  {
    VMatrix out = GE(g,0);
    Real G.out = VMatSum(g$*out);
    If(G.out<=0, 0, -_.penalty*G.out) 
  }

  Real _penalty_log_likelihood(VMatrix g)
};

////////////////////////////////////////////////////////////////////////////////
Class @Eq : @Constraint
////////////////////////////////////////////////////////////////////////////////
{
  //////////////////////////////////////////////////////////////////////////////
  // Returns 0 if g==0 and a huge negative value in other case
  Real _penalty_log_likelihood(VMatrix g)
  //////////////////////////////////////////////////////////////////////////////
  {
    _penalty_eval(g^2)
  }
};

////////////////////////////////////////////////////////////////////////////////
// Restricciones deterministas de punto fijo
// x = cte
Class @Eq.Fixed : @Eq
////////////////////////////////////////////////////////////////////////////////
{
  VMatrix _.point;
  Real _log_likelihood(VMatrix x)
  {
    _penalty_log_likelihood(x-_.point) 
  }
};

////////////////////////////////////////////////////////////////////////////////
// Restricciones deterministas de punto fijo
// A*x = a
Class @Eq.Linear : @Eq
////////////////////////////////////////////////////////////////////////////////
{
  VMatrix _.A;
  VMatrix _.a;         
  Real _log_likelihood(VMatrix x)
  {
    _penalty_log_likelihood(_.A*x-_.a) 
  }
};

////////////////////////////////////////////////////////////////////////////////
Class @Ineq : @Constraint
////////////////////////////////////////////////////////////////////////////////
{
  //////////////////////////////////////////////////////////////////////////////
  // Returns 0 if g<=0 and a huge negative value in other case
  Real _penalty_log_likelihood(VMatrix g)
  //////////////////////////////////////////////////////////////////////////////
  {
    _penalty_eval(g)
  }
};

////////////////////////////////////////////////////////////////////////////////
// Restricciones deterministas de dominio (intervalo o rango)
// _.lower <= x <= _.upper
Class @Ineq.Domain : @Ineq
////////////////////////////////////////////////////////////////////////////////
{
  VMatrix _.lower;
  VMatrix _.upper;         
  Real _log_likelihood(VMatrix x)
  {
    VMatrix g1 = x - _.upper;
    VMatrix g2 = _.lower - x;
    _penalty_log_likelihood(g1<<g2) 
  }
};

////////////////////////////////////////////////////////////////////////////////
// Restricciones deterministas de desigualdad lineal 
// _.A x <= _.a
Class @Ineq.Linear : @Ineq
////////////////////////////////////////////////////////////////////////////////
{
  VMatrix _.A;
  VMatrix _.a;         
  Real _log_likelihood(VMatrix x)
  {
    _penalty_log_likelihood(_.A*x-_.a) 
  }
};

}}}

=== Modelo de regresi�n normal con prior conjugado para la varianza ===

[[Image(source:tolp/OfficialTolArchiveNetwork/MWG/doc/esquema.01.dib)]]

{{{
#!java
////////////////////////////////////////////////////////////////////////////////
// Modelo de regresi�n normal con prior conjugado para la varianza
@Model Create.LinReg.Normal(
  VMatrix Y, //Output
  VMatrix X  //Input     )
////////////////////////////////////////////////////////////////////////////////
{
  @Model model = @Model::New(?);
  MWG::@Model model = MWG::@Model::New(?);

  Real m = VRows(Y);
  Real n = VColumns(X);
  
  MWG::@PRI.InvScaChiSquare sigma2 = [[
    VMatrix _.drawn = Mat2VMat(Col(sigma2.0));
    Real _.freeDeg = m;
    Real _.scale = sigma2.0 ]];

  MWG::@PRI.Normal residuals = [[
    VMatrix _.drawn = residuals.0;
    Real _.nu = 0;
    Real _.sigma = Sqrt(sigma2.0) ]];
  
  MWG::@RVOV.LinReg beta = [[
    VMatrix _.drawn = beta.0;
    Set _.Y = [[Y]];
    Set _.X = [[X]];
    Set _.e = [[residuals]] ]];

  Real model::addRandVar(beta,   True);
  Real model::addRandVar(sigma2, True);

  Real { model::addRelation(MWG::@Relation.Assign rel = [[
    Text _.type = "Real";
    Text _.from = "sigma2";
    Text _.to = "beta";
    Text _.expr = "((beta::_.e)[1])::_.sigma := Sqrt(VMatDat(sigma2::_.drawn,1,1))" ]]) };

  Real { model::addRelation(MWG::@Relation.Assign rel = [[
    Text _.type = "Real";
    Text _.from = "beta";
    Text _.to = "sigma2";
    Text _.expr = "sigma2::_.scale := VMatSum((beta::filter(beta::_.drawn))^2)" ]]) };
  model
};
}}}
