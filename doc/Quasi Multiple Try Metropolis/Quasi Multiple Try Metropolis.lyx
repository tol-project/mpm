#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass amsbook
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Quasi Multiple Try Metropolis
\end_layout

\begin_layout Author
Víctor de Buen Remiro
\end_layout

\begin_layout Author
©Bayes Decision, 2014
\end_layout

\begin_layout Email
vdebuen@bayesforecast.com
\end_layout

\begin_layout Abstract
MCMC methods for expensive black-box and high dimensional distributions
 usually present a lot of problems of convergence, specially when there
 are non linear constraints, multicollinearity or multiple local maxima.
 This kind of distribution arise on modelling problem coming from real processes.
 Here we will develop some methods to apply to this kind of target distributions
 that aims to use historical Markov chain in order to minimize the number
 of evaluations preserving the detailed balance equation.
 We will merge Generalized Multiple Try Metropolis Hastings (GMTM), and
 Inverse Distance Weighting (IDW) or Radial Basis Functions (RBF) interpolation,
 applied over historic chain in order to get both a good mixing and a good
 exploration of the target, while minimizing the number of evaluations for
 each iteration, and always preserving the detailed balance equation.
\end_layout

\begin_layout Abstract
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Part
Unconstrained distributions
\end_layout

\begin_layout Standard
Although real problems frequently are constrained in a large variety of
 ways, almost all articles about MCMC methods work only for unconstrained
 distributions, so at first we will develop our methods for these ones,
 and after we will adjust them to constrained problems, which introduce
 a lot of numerical issues.
\end_layout

\begin_layout Chapter
MCMC algorithms
\end_layout

\begin_layout Standard
Here we will present a set of MCMC known algorithms satisfying detailed
 balance equations, which is a sufficient condition to ensure asymptotic
 convergence, and which only requires evaluations of the target log-density
 but a constant, that can be defined as a black-box arbitrary non-negative
 function.
\end_layout

\begin_layout Section
MH: Metropolis-Hastings
\end_layout

\begin_layout Standard
This is the simplest way to obtain a Markov Chain from an arbitrary posterior
 density.
\end_layout

\begin_layout Standard
Let be 
\begin_inset Formula $\pi\left(x\right)$
\end_inset

 the posterior density of a multivariate random variable 
\begin_inset Formula $x\in\mathbb{R}^{d}$
\end_inset

 which we are able to evaluate but no to draw in a direct way and is always
 positive, i.e.
 
\begin_inset Formula 
\[
\pi\left(x\right)>0\:\forall\: x\in\mathbb{R}^{d}
\]

\end_inset

.
\end_layout

\begin_layout Standard
Let be 
\begin_inset Formula $T\left(x,y\right)$
\end_inset

 the proposal density, meaning the density of probability of moving from
 
\begin_inset Formula $x$
\end_inset

 to 
\begin_inset Formula $y$
\end_inset

, and that we are able to evaluate and draw in a direct and efficient way
 satisfying 
\begin_inset Formula 
\[
T\left(x,y\right)>0\Longleftrightarrow T\left(y,x\right)>0
\]

\end_inset

.
\end_layout

\begin_layout Standard
Let be 
\begin_inset Formula $x_{t}\in\mathbb{R}^{d}$
\end_inset

 the current state at iteration 
\begin_inset Formula $t$
\end_inset

 
\end_layout

\begin_layout Standard
Metropolis-Hastings (MH) algorithm steps are
\end_layout

\begin_layout Enumerate
Generate the current candidate following the proposal density 
\begin_inset Formula $y\sim T\left(x_{t},\cdot\right)$
\end_inset

 
\end_layout

\begin_layout Enumerate
Calculate the acceptance probability
\begin_inset Formula 
\[
\alpha=\min\left\{ 1,\frac{\pi\left(y\right)}{\pi\left(x_{t}\right)}\frac{T\left(y,x_{t}\right)}{T\left(x_{t},y\right)}\right\} 
\]

\end_inset


\end_layout

\begin_layout Enumerate
We generate a unity uniform random variable 
\begin_inset Formula $r\sim U\left[0,1\right]$
\end_inset

 and we will accept the candidate if and only if is lower or equal than
 acceptance probability
\begin_inset Formula 
\[
x_{t+1}=\begin{cases}
y & \Longleftrightarrow r\leq\alpha\\
x_{t} & \Longleftrightarrow r>\alpha
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Standard
Note that
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\alpha=\min\left\{ 1,\exp\left(\ln\pi\left(y\right)+\ln T\left(y,x_{t}\right)-\ln\pi\left(x_{t}\right)-\ln T\left(x_{t},y\right)\right)\right\} 
\]

\end_inset

is a more numerically stable expression of acceptance probability due to
 exponential function should take intractable values in discrete arithmetic.
\end_layout

\begin_layout Standard
So we only need to known how to calculate logarithm of posterior and proposal
 densities but a constant term.
\end_layout

\begin_layout Standard
Usually, the proposal distribution is taken as a random walk 
\begin_inset Formula 
\[
T_{RW,\sigma^{2}}\left(x,y\right)\sim N\left(x,\sigma^{2}I_{d}\right)
\]

\end_inset

a scaled random walk 
\begin_inset Formula 
\[
T_{SRW,\sigma^{2},D}\left(x,y\right)\sim N\left(x,\sigma^{2}D\right)\wedge D_{i,j}=0\forall i\neq j
\]

\end_inset

 when some variables are very different in absolute value from others, or
 a covariated random walk 
\begin_inset Formula 
\[
T_{CRW,\Sigma}\left(x,y\right)\sim N\left(x,\Sigma\right)
\]

\end_inset

 when target presents high correlations.
\end_layout

\begin_layout Standard
Main problems of this algorithm are:
\end_layout

\begin_layout Itemize
Step size (or covariance matrix ) of the proposal distribution is determinant
 for the convergence, and they may be tuned using complicated adaptive methods.
\end_layout

\begin_layout Itemize
Even if the problems with scales and correlations are avoided, the step
 size will increase as the square root of the number of dimensions, that
 almost always land in regions of low probability, and therefore will be
 rejected.
 We can take the change 
\begin_inset Formula $\sigma^{2}\rightarrow\sigma^{2}/d$
\end_inset

, but the exploration of the probability space becomes increasingly slow
 for high dimensional problems.
\end_layout

\begin_layout Section
MTM: Multiple Try Metropolis
\end_layout

\begin_layout Standard
Multiple-try Metropolis is a sampling method that is a modified form of
 the Metropolis-Hastings method, first presented at 
\begin_inset CommandInset citation
LatexCommand cite
key "Liu-Liang-Wong 2000"

\end_inset

.
 It is designed to help the sampling trajectory converge faster, by increasing
 both the step size and the acceptance rate.
 
\end_layout

\begin_layout Standard
Let be an arbitrary non-negative bounded symmetric function
\begin_inset Formula 
\[
0\leq\lambda\left(x,y\right)=\lambda\left(y,x\right)<\infty
\]

\end_inset


\end_layout

\begin_layout Standard
Let be the weighting function defined as 
\begin_inset Formula 
\[
w\left(y,x\right)=\pi\left(y\right)T\left(y,x\right)\lambda\left(y,x\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Multiple Try Metropolis (MTM) algorithm steps are
\end_layout

\begin_layout Enumerate
Draw 
\begin_inset Formula $k$
\end_inset

 independent trial proposal as 
\begin_inset Formula 
\[
y_{1},\ldots,y_{k}\sim T\left(x_{t},\cdot\right)
\]

\end_inset


\end_layout

\begin_layout Enumerate
Select candidate 
\begin_inset Formula $y$
\end_inset

 among set 
\begin_inset Formula $\left\{ y_{1},\ldots,y_{k}\right\} $
\end_inset

 with probability proportional to 
\begin_inset Formula $w\left(y_{j},x_{t}\right)$
\end_inset

, i.e., with assigning to each trial proposal the probability
\begin_inset Formula 
\[
p_{y_{j}}=\frac{w\left(y_{j},x_{t}\right)}{\overset{k}{\underset{i=1}{\sum}}w\left(y_{i},x_{t}\right)}
\]

\end_inset


\end_layout

\begin_layout Enumerate
Draw 
\begin_inset Formula $k-1$
\end_inset

 independent reverted points
\begin_inset Formula 
\[
z_{1},\ldots,z_{k-1}\sim T\left(y,\cdot\right)
\]

\end_inset


\end_layout

\begin_layout Enumerate
Sets 
\begin_inset Formula $z_{k}=x_{t}$
\end_inset


\end_layout

\begin_layout Enumerate
Accept 
\begin_inset Formula $y$
\end_inset

 with probability
\begin_inset Formula 
\[
\alpha=\min\left\{ 1,\frac{\overset{k}{\underset{i=1}{\sum}}w\left(y_{i},x_{t}\right)}{\overset{k}{\underset{i=1}{\sum}}w\left(z_{i},y\right)}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
Main problem of MTM is that requires 
\begin_inset Formula $2k-1$
\end_inset

 evaluations of target in each iteration, that could be prohibitive if is
 too expensive to calculate it.
 Normally, the number of tries must be tuned in some way in order to obtain
 a true gain with regard to a 
\begin_inset Formula $\left(2k-1\right)$
\end_inset

-thinned Metropolis-Hastings.
\end_layout

\begin_layout Section
GMTM: Generalized Multiple Try Metropolis
\end_layout

\begin_layout Standard
Now we can have 
\begin_inset Formula $k$
\end_inset

 eventually different proposal densities 
\begin_inset Formula $T_{i}\left(x,y\right)$
\end_inset

 and 
\begin_inset Formula $k$
\end_inset

 arbitrary weighting bounded positive functions 
\begin_inset Formula 
\[
0<w_{i}\left(y,x\right)<\infty
\]

\end_inset


\end_layout

\begin_layout Standard
Generalized Multiple Try Metropolis (GMTM) algorithm, as defined in 
\begin_inset CommandInset citation
LatexCommand cite
key "Martino-Read 2013"

\end_inset

, follows these steps
\end_layout

\begin_layout Enumerate
Draw 
\begin_inset Formula $k$
\end_inset

 independent trial proposal as 
\begin_inset Formula 
\[
y_{i}\sim T_{i}\left(x_{t},\cdot\right)\;\forall i=1,\ldots,k
\]

\end_inset


\end_layout

\begin_layout Enumerate
Select a candidate 
\begin_inset Formula $y=y_{j}$
\end_inset

 with probability proportional to 
\begin_inset Formula $w\left(y_{j},x_{t}\right)$
\end_inset

, i.e.
\begin_inset Formula 
\[
p_{y_{j}}=\frac{w_{j}\left(y_{j},x_{t}\right)}{\overset{k}{\underset{i=1}{\sum}}w_{i}\left(y_{i},x_{t}\right)}
\]

\end_inset


\end_layout

\begin_layout Enumerate
Draw 
\begin_inset Formula $k-1$
\end_inset

 independent reference points
\begin_inset Formula 
\[
z_{i}\sim T_{i}\left(y,\cdot\right)\;\forall i\neq j
\]

\end_inset


\end_layout

\begin_layout Enumerate
Set 
\begin_inset Formula $z_{j}=x_{t}$
\end_inset


\end_layout

\begin_layout Enumerate
Define
\begin_inset Formula 
\[
p_{x_{t}}=\frac{w_{j}\left(x_{t},y\right)}{\overset{k}{\underset{i=1}{\sum}}w_{i}\left(z_{i},y\right)}
\]

\end_inset


\end_layout

\begin_layout Enumerate
Accept 
\begin_inset Formula $y=y_{j}$
\end_inset

 with probability
\begin_inset Formula 
\[
\alpha=\min\left\{ 1,\frac{\pi\left(y\right)}{\pi\left(x_{t}\right)}\frac{T_{j}\left(y,x_{t}\right)p_{x_{t}}}{T_{j}\left(x_{t},y\right)p_{y}}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
Note that we could merge proposal distributions of different kinds, as random
 walks of different deviations and unsymmetrical, multipole or fixed distributio
ns.
 For example we could take random walks of growing step sizes 
\begin_inset Formula $\delta i$
\end_inset

 as 
\begin_inset Formula 
\[
T_{i}\left(x,y\right)\sim N\left(x,-\delta^{2}i^{2}I_{d}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
The main advantage of this approach, when target density is very expensive,
 is that we don't need to evaluate it at every trial proposal and every
 reference point, but only in the selected candidate.
 Effectiveness of this method relays on both proposals and weightings selection.
\end_layout

\begin_layout Section
QMTM: Quasi Multiple Try Metropolis
\end_layout

\begin_layout Standard
Let be an inexpensive positive approximation of the target 
\begin_inset Formula 
\[
\begin{array}{c}
F\left(x\right)\approx\pi\left(x\right)\\
F\left(x\right)>0\:\forall\: x\in\mathbb{R}^{d}
\end{array}
\]

\end_inset

Then
\begin_inset Formula 
\[
A\left(y,x\right)=\frac{F\left(y\right)}{F\left(x\right)}\approx\frac{\pi\left(y\right)}{\pi\left(x\right)}
\]

\end_inset

is an approximation of the quotient of densities.
 In practical terms it's a better idea to get an approximation of logarithm
 of target
\begin_inset Formula 
\[
\ln F\left(x\right)\approx\ln\pi\left(x\right)
\]

\end_inset


\begin_inset Formula 
\[
\ln A\left(y,x\right)=\ln F\left(y\right)-\ln F\left(x\right)\approx\ln\pi\left(y\right)-\ln\pi\left(x\right)
\]

\end_inset


\end_layout

\begin_layout Standard
If we forze proposals densities to be always positive 
\begin_inset Formula 
\[
T_{i}\left(x,y\right)>0\:\forall\: x,y\in\mathbb{R}^{d}
\]

\end_inset

 then the functions
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
w_{i}\left(y,x\right)=\frac{A\left(y,x\right)}{T_{i}\left(x,y\right)}
\]

\end_inset

are always positive and bounded, so we can to use GMTM with these weighting
 functions.
\end_layout

\begin_layout Standard
Since
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
p_{y}=\frac{A\left(y,x_{t}\right)}{T_{j}\left(x_{t},y\right)}\left(\overset{k}{\underset{i=1}{\sum}}\frac{A\left(y_{i},x_{t}\right)}{T_{i}\left(x_{t},y_{i}\right)}\right)^{-1}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
p_{x_{t}}=\frac{A\left(x_{t},y\right)}{T_{j}\left(y,x_{t}\right)}\left(\overset{k}{\underset{i=1}{\sum}}\frac{A\left(z_{i},y\right)}{T_{i}\left(y,z_{i}\right)}\right)^{-1}
\]

\end_inset

we obtain the acceptance probability
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\alpha=\min\left\{ 1,\frac{\pi\left(y\right)}{\pi\left(x_{t}\right)}\frac{T_{j}\left(y,x_{t}\right)p_{x_{t}}}{T_{j}\left(x_{t},y\right)p_{y}}\right\} =\min\left\{ 1,\frac{\pi\left(y\right)T_{j}\left(y,x_{t}\right)}{\pi\left(x_{t}\right)T_{j}\left(x_{t},y\right)}\frac{\frac{A\left(x_{t},y\right)}{T_{j}\left(y,x_{t}\right)}}{\frac{A\left(y,x_{t}\right)}{T_{j}\left(x_{t},y\right)}}\frac{\overset{k}{\underset{i=1}{\sum}}\frac{A\left(y_{i},x_{t}\right)}{T_{i}\left(x_{t},y_{i}\right)}}{\overset{k}{\underset{i=1}{\sum}}\frac{A\left(z_{i},y\right)}{T_{i}\left(y,z_{i}\right)}}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
Simplifying this expression
\begin_inset Formula 
\[
\alpha=\min\left\{ 1,\frac{\pi\left(y\right)}{\pi\left(x_{t}\right)}\frac{A\left(x_{t},y\right)}{A\left(y,x_{t}\right)}\frac{\overset{k}{\underset{i=1}{\sum}}\frac{A\left(y_{i},x_{t}\right)}{T_{i}\left(x_{t},y_{i}\right)}}{\overset{k}{\underset{i=1}{\sum}}\frac{A\left(z_{i},y\right)}{T_{i}\left(y,z_{i}\right)}}\right\} =\min\left\{ 1,\frac{\pi\left(y\right)}{\pi\left(x_{t}\right)}\frac{\frac{F\left(x_{t}\right)}{F\left(y\right)}\frac{1}{F\left(x_{t}\right)}}{\frac{F\left(y\right)}{F\left(x_{t}\right)}\frac{1}{F\left(y\right)}}\frac{\overset{k}{\underset{i=1}{\sum}}\frac{F\left(y_{i}\right)}{T_{i}\left(x_{t},y_{i}\right)}}{\overset{k}{\underset{i=1}{\sum}}\frac{F\left(z_{i}\right)}{T_{i}\left(y,z_{i}\right)}}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
we obtain
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\alpha=\min\left\{ 1,\frac{\pi\left(y\right)}{\pi\left(x_{t}\right)}\frac{F\left(x_{t}\right)}{F\left(y\right)}\frac{\overset{k}{\underset{i=1}{\sum}}\frac{F\left(y_{i}\right)}{T_{i}\left(x_{t},y_{i}\right)}}{\overset{k}{\underset{i=1}{\sum}}\frac{F\left(z_{i}\right)}{T_{i}\left(y,z_{i}\right)}}\right\} 
\]

\end_inset

where
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{\pi\left(y\right)}{\pi\left(x_{t}\right)}\frac{F\left(x_{t}\right)}{F\left(y\right)}\approx1
\]

\end_inset


\end_layout

\begin_layout Standard
In order to avoid numerical problems we will define
\begin_inset Formula 
\[
F^{+}=\max\left\{ \max\left\{ F\left(y_{i}\right)\right\} ,\max\left\{ F\left(z_{i}\right)\right\} \right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
T^{+}=\max\left\{ \max\left\{ T_{i}\left(x_{t},y_{i}\right)\right\} ,\max\left\{ T_{i}\left(y,z_{i}\right)\right\} \right\} 
\]

\end_inset

and taken account that
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\overset{k}{\underset{i=1}{\sum}}\frac{F\left(y_{i}\right)}{T_{i}\left(x_{t},y_{i}\right)}=\frac{F^{+}}{T^{+}}\overset{k}{\underset{i=1}{\sum}}\exp\left(\ln F\left(y_{i}\right)-\ln F^{+}+\ln T_{i}\left(x_{t},y_{i}\right)-\ln T^{+}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\overset{k}{\underset{i=1}{\sum}}\frac{F\left(z_{i}\right)}{T_{i}\left(y,z_{i}\right)}=\frac{F^{+}}{T^{+}}\overset{k}{\underset{i=1}{\sum}}\exp\left(\ln F\left(z_{i}\right)-\ln F^{+}+\ln T_{i}\left(y,z_{i}\right)-\ln T^{+}\right)
\]

\end_inset

we obtain a robust expression of acceptance probability
\begin_inset Formula 
\[
\alpha=\min\left\{ 1,\exp\left(L_{1}\right)\frac{\overset{k}{\underset{i=1}{\sum}}\exp\left(L_{2,i}\right)}{\overset{k}{\underset{i=1}{\sum}}\exp\left(L_{3,i}\right)}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
L_{1}=\ln\pi\left(y\right)+\ln F\left(x_{t}\right)-\ln\pi\left(x_{t}\right)-\ln F\left(y\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
L_{2,i}=\ln F\left(y_{i}\right)-\ln F^{+}+\ln T_{i}\left(x_{t},y_{i}\right)-\ln T^{+}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
L_{3,i}=\ln F\left(z_{i}\right)-\ln F^{+}+\ln T_{i}\left(y,z_{i}\right)-\ln T^{+}
\]

\end_inset


\end_layout

\begin_layout Chapter
Multivariate approximation algorithms
\end_layout

\begin_layout Standard
Approximation of multivariate functions is a very hard issue, that becomes
 almost intractable with high dimensional ones.
 There are just a couple of methods that are relatively able to handle with
 them: Inverse Distance Weighting (IDW) interpolation, also called Sheppard's
 method, and Radial Basis Functions interpolation (RBF).
\end_layout

\begin_layout Section
IDW: Inverse Distance Weighting
\end_layout

\begin_layout Standard
Let be 
\begin_inset Formula $f\left(x\right):\Omega\in\mathbb{R}^{d}\rightarrow\mathbb{R}$
\end_inset

 an arbitrary function which we have calculated previously for a set of
 
\begin_inset Formula $m$
\end_inset

 points of reference 
\begin_inset Formula 
\[
f_{j}=f\left(x_{j}\right)\;\forall j=1,\ldots,m
\]

\end_inset


\end_layout

\begin_layout Standard
The Inverse Distance Weighting (IDW) interpolating function or order 
\begin_inset Formula $p>0$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
S\left(x\right)=\frac{\overset{m}{\underset{j=1}{\sum}}w_{j}\left(x\right)f_{j}}{\overset{m}{\underset{j=1}{\sum}}w_{j}\left(x\right)}
\]

\end_inset

where weighting functions are inversely proportional to euclidean distance
 to reference points
\begin_inset Formula 
\[
w_{j}\left(x\right)=\left\Vert x-x_{j}\right\Vert _{2}^{-p}=\left(\overset{d}{\underset{s=1}{\sum}}\left(x^{\left(s\right)}-x_{j}^{\left(s\right)}\right)^{2}\right)^{-\frac{p}{2}}
\]

\end_inset


\end_layout

\begin_layout Standard
There is a fast version based on k-Nearest Neighbour (KNN) algorithm where
 just the 
\begin_inset Formula $k$
\end_inset

-closest to 
\begin_inset Formula $x$
\end_inset

 reference points will be employed to build 
\begin_inset Formula $\tilde{f}\left(x\right)$
\end_inset

.
\end_layout

\begin_layout Standard
This method is very easy to implement and is very cheaper in resources but
 it doesn't work very fine and gives no information about gradient and hessian.
\end_layout

\begin_layout Section
RBF: Radial Basis Functions
\end_layout

\begin_layout Standard
A Radial Basis Function or dimension 
\begin_inset Formula $d$
\end_inset

 and degreee 
\begin_inset Formula $n$
\end_inset

 has this expression
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
R_{d,n}:\mathbb{R}^{d}\rightarrow\mathbb{R}
\]

\end_inset


\begin_inset Formula 
\[
R_{d,n}\left(x\right)=P_{n}\left(x\right)+\overset{m}{\underset{j=1}{\sum}}\lambda_{j}\phi\left(\left\Vert x-x_{j}\right\Vert _{2}\right)
\]

\end_inset

where the basis function 
\begin_inset Formula $\phi$
\end_inset

 is a continuous real function 
\begin_inset Formula $\phi:\mathbb{R}\rightarrow\mathbb{R}$
\end_inset

 and 
\begin_inset Formula $P_{n}\left(x\right)$
\end_inset

 is a multivariate polynomial of degree 
\begin_inset Formula $n$
\end_inset

.
 In high dimensional problems polynomial mast be lineal in order to avoid
 an excesive number of parameters
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
R\left(x\right)=R_{d,1}\left(x\right)=a_{0}+a^{T}x+\overset{m}{\underset{j=1}{\sum}}\lambda_{j}\phi\left(\left\Vert x-x_{j}\right\Vert _{2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
There are a lot of proposed basis functions but we will restrict to cubic
 spline (CS) 
\begin_inset Formula 
\[
\phi_{CS}\left(r\right)=r^{3}
\]

\end_inset

Thin-Plate Spline (TPS)
\begin_inset Formula 
\[
\phi_{TPS}\left(r\right)=r^{2}\ln r
\]

\end_inset

and Multi-Quadric (MQ)
\begin_inset Formula 
\[
\phi_{MQ,\varepsilon}\left(r\right)=\sqrt{1+\left(\varepsilon r\right)^{2}}
\]

\end_inset


\end_layout

\begin_layout Standard
In order to fit an arbitrary function 
\begin_inset Formula $f\left(x\right):\Omega\in\mathbb{R}^{d}\rightarrow\mathbb{R}$
\end_inset

 which we have calculated previously for a set of 
\begin_inset Formula $m$
\end_inset

 different points of reference 
\begin_inset Formula 
\[
f_{j}=f\left(x_{j}\right)\;\forall j=1,\ldots,m
\]

\end_inset

 we should to solve a system of 
\begin_inset Formula $1+d+m$
\end_inset

 parameters and only 
\begin_inset Formula $m$
\end_inset

 equations
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left(\begin{array}{ccccccc}
1 & x_{1}^{\left(1\right)} & \cdots & x_{1}^{\left(d\right)} & \phi_{1,1} & \cdots & \phi_{1,m}\\
\vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots\\
1 & x_{m}^{\left(d\right)} & \cdots & x_{m}^{\left(d\right)} & \phi_{m,1} & \cdots & \phi_{m,m}
\end{array}\right)\left(\begin{array}{c}
a_{0}\\
a_{1}\\
\vdots\\
a_{d}\\
\lambda_{1}\\
\vdots\\
\lambda_{m}
\end{array}\right)=\left(\begin{array}{c}
f_{1}\\
\vdots\\
f_{m}
\end{array}\right)
\]

\end_inset

where symmetric crossing basis 
\begin_inset Formula $\phi_{i,j}$
\end_inset

 are defined as 
\begin_inset Formula 
\[
\phi_{i,j}=\phi_{j,i}=\phi\left(\left\Vert x_{i}-x_{j}\right\Vert _{2}\right)\;\forall i,j=1,\ldots,m
\]

\end_inset


\end_layout

\begin_layout Standard
So we can add 
\begin_inset Formula $1+d$
\end_inset

 symmetric conditions as
\begin_inset Formula 
\[
\overset{m}{\underset{j=1}{\sum}}\lambda_{j}=0
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\overset{m}{\underset{j=1}{\sum}}\lambda_{j}x_{j}^{\left(s\right)}=0\:\forall\: s=1,\ldots,d
\]

\end_inset


\end_layout

\begin_layout Standard
in order to get a symmetric linear system 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left(\begin{array}{ccccccc}
0 & 0 & \cdots & 0 & 1 & \cdots & 1\\
0 & 0 & \cdots & 0 & x_{1}^{\left(1\right)} & \cdots & x_{m}^{\left(1\right)}\\
\vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots\\
0 & 0 & \cdots & 0 & x_{1}^{\left(d\right)} & \cdots & x_{m}^{\left(d\right)}\\
1 & x_{1}^{\left(1\right)} & \cdots & x_{1}^{\left(d\right)} & \phi_{1,1} & \cdots & \phi_{1,m}\\
\vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots\\
1 & x_{m}^{\left(d\right)} & \cdots & x_{m}^{\left(d\right)} & \phi_{1,m} & \cdots & \phi_{m,m}
\end{array}\right)\left(\begin{array}{c}
a_{0}\\
a_{1}\\
\vdots\\
a_{d}\\
\lambda_{1}\\
\vdots\\
\lambda_{m}
\end{array}\right)=\left(\begin{array}{c}
0\\
0\\
\vdots\\
0\\
f_{1}\\
\vdots\\
f_{m}
\end{array}\right)
\]

\end_inset

that is nonsingular if the points of reference are not situated on one line
 
\begin_inset CommandInset citation
LatexCommand cite
key "Powell 1998"

\end_inset

.
\end_layout

\begin_layout Section
Approximation by means of mixtures of densities
\end_layout

\begin_layout Standard
If we have a set of density functions 
\begin_inset Formula $\left\{ \theta_{i}\right\} _{i=1\ldots m}$
\end_inset

 that we are able to draw and evaluate in a very fast way, then we can build
 mixtures of density by means of convex combinations of them
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
M\left(x\right)=\overset{m}{\underset{j=1}{\sum}}\lambda_{j}\theta_{j}\left(x\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\lambda_{j}\geq0\:\wedge\:\overset{m}{\underset{j=1}{\sum}}\lambda_{j}=1
\]

\end_inset


\end_layout

\begin_layout Standard
Then, this function is also a density that we can draw in this simple way
\end_layout

\begin_layout Enumerate
Select a density 
\begin_inset Formula $j$
\end_inset

 with probability 
\begin_inset Formula $\lambda_{j}$
\end_inset


\end_layout

\begin_layout Enumerate
Draw a realization of selected density 
\begin_inset Formula $x\sim\theta_{j}$
\end_inset


\end_layout

\begin_layout Standard
In order to be able to impose these convex conditions, we can renounce to
 interpolation and try just functional approximation adding more sample
 length 
\begin_inset Formula $S>m$
\end_inset

 and take the solution of constrained minimum squares 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\underset{\lambda}{\min}\left\Vert \left(\begin{array}{c}
f_{1}\\
\vdots\\
f_{S}
\end{array}\right)-\left(\begin{array}{ccc}
\theta_{1,1} & \cdots & \theta_{1,m}\\
\vdots & \vdots & \vdots\\
\theta_{1,S} & \cdots & \theta_{m,S}
\end{array}\right)\left(\begin{array}{c}
\lambda_{1}\\
\vdots\\
\lambda_{m}
\end{array}\right)\right\Vert _{2}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
subject to
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\lambda_{j}\geq0\:\wedge\:\overset{m}{\underset{j=1}{\sum}}\lambda_{j}=1
\]

\end_inset


\end_layout

\begin_layout Standard
In order to avoid numerical issues, we can reescale output and input sides
 by
\begin_inset Formula 
\[
f^{+}=\underset{i=1\ldots S}{\max}\left\{ f_{i}\right\} 
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\theta^{+}=\underset{\begin{array}{c}
i=1\ldots S\\
j=1\ldots m
\end{array}}{\max}\left\{ \theta_{i,j}\right\} 
\]

\end_inset

and apply the change of variable
\begin_inset Formula 
\[
\lambda'_{j}=\frac{\theta^{+}}{f^{+}}\lambda_{j}
\]

\end_inset


\end_layout

\begin_layout Standard
Then we can write this new constrained minimum squares solution 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\underset{\lambda'}{\min}\left\Vert \frac{1}{f^{+}}\left(\begin{array}{c}
f_{1}\\
\vdots\\
f_{S}
\end{array}\right)-\frac{1}{\phi^{+}}\left(\begin{array}{ccc}
\theta_{1,1} & \cdots & \theta_{1,m}\\
\vdots & \vdots & \vdots\\
\theta_{1,S} & \cdots & \theta_{m,S}
\end{array}\right)\left(\begin{array}{c}
\lambda'_{1}\\
\vdots\\
\lambda'_{m}
\end{array}\right)\right\Vert _{2}^{2}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\lambda'_{j}\geq0\:\wedge\:\overset{m}{\underset{j=1}{\sum}}\lambda'_{j}=\frac{\theta^{+}}{f^{+}}
\]

\end_inset

that can be expressed in terms of log-densities
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{f_{j}}{f^{+}}=\exp\left(\ln\left(f_{1}\right)-\ln\left(f^{+}\right)\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\frac{\theta_{i,j}}{\phi^{+}}=\exp\left(\ln\left(\theta_{i,j}\right)-\ln\left(\theta^{+}\right)\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Now we will propose some posible sets of densities.
 Firstly, we can select a set of overdispersed reference points 
\begin_inset Formula $\left\{ u_{i}\right\} _{i=1\ldots m'}$
\end_inset

 and search the 
\begin_inset Formula $d+1$
\end_inset

 closest neighbourgs of each one 
\begin_inset Formula $\left\{ v_{i,j}\right\} _{i=1\ldots m'}$
\end_inset

, it self included, by means of KNN algorithm
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left\Vert x_{h}-x_{i}\right\Vert _{2}\geq\left\Vert v_{i,j}-x_{i}\right\Vert _{2}\:\forall\: x_{h}\neq v_{i,j}
\]

\end_inset


\end_layout

\begin_layout Standard
Then we can take the average of 
\begin_inset Formula $\left\{ v_{i,j}\right\} _{i=1\ldots m\text{'}}$
\end_inset


\begin_inset Formula 
\[
w_{i}=\frac{1}{2}\overset{d}{\underset{j=1}{\sum}}v_{i,j}
\]

\end_inset

and define uncorrelated
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\theta_{i}\sim N\left(w_{i},D_{i}\right)\:\forall\: i=1,2,\ldots,m\text{'}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
D_{i}^{h,h}=\delta\left(\underset{j=1\ldots d+1}{\max}\left\{ v_{i,j}^{h}\right\} -\underset{j=1\ldots d+1}{\min}\left\{ v_{i,j}^{h}\right\} \right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
D_{i}^{h,k}=0\:\forall\: k\neq h
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\theta_{i+(g-1)m'}\sim N\left(w_{i},gD_{i}\right)\:\forall\: g=2,3,\ldots,G
\]

\end_inset


\end_layout

\begin_layout Part
Constrained distributions
\end_layout

\begin_layout Standard
Now we will deal with distributions constrained to a region 
\begin_inset Formula $\Omega$
\end_inset

 of 
\begin_inset Formula $d$
\end_inset

-dimensional real vectoral space
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\pi\left(x\right)>0\:\forall\: x\in\Omega\subset\mathbb{R}^{d}
\]

\end_inset


\end_layout

\begin_layout Standard
In the most general case, region 
\begin_inset Formula $\Omega$
\end_inset

 is an arbitrary region that can be defined by the the intersection of the
 negative images of a set of arbitrary black box constraining functions
 
\begin_inset Formula $g_{r}:\mathbb{R}^{d}\rightarrow\mathbb{R}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
x\in\Omega\Longleftrightarrow g_{r}\left(x\right)\leq0\forall\: r=1,2,\ldots,R
\]

\end_inset


\end_layout

\begin_layout Standard
When all constraints are lineal region 
\begin_inset Formula $\Omega$
\end_inset

 is a convex polytope that can be defined as a matricial linear inequality
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
x\in\Omega\Longleftrightarrow Ax\leq a
\]

\end_inset


\end_layout

\begin_layout Standard
Region 
\begin_inset Formula $\Omega$
\end_inset

 will be bounded if it is contained ina a poly-cuboid, it's to say if there
 are finite bounds for all dimensions of each point in 
\begin_inset Formula $\Omega$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
x\in\Omega\Rightarrow\exists l_{k},u_{k}\in\mathbb{R}\,\bot\, l_{k}\leq x\leq u_{k}\forall\: k=1,2,\ldots,d
\]

\end_inset


\end_layout

\begin_layout Chapter
A Quasi-Multiple Try Metropolis simulator with univariant interpolated proposal
 over a random ray in a bounded convex polytope
\end_layout

\begin_layout Standard
Let be a probability distribution 
\begin_inset Formula $\pi$
\end_inset

 in a convex bounded polytope
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\pi\left(x\right)=0\:\forall x\notin\Omega\subset\mathbb{R}^{d}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
x\in\Omega\Longleftrightarrow Ax\leq a
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
x\in\Omega\Rightarrow\exists l_{k},u_{k}\in\mathbb{R}\,\bot\, l_{k}\leq x\leq u_{k}\forall\: k=1,2,\ldots,d
\]

\end_inset


\end_layout

\begin_layout Chapter
Strategies for MCMC convergence
\end_layout

\begin_layout Standard
Convergence of MCMC methods are always warranted but there are no known
 limitation about how large could be the convergence period, i.e., the length
 of simulations to be burned.
 Specially when chain starts from or falls in a region with very flat density,
 or there are complicated constrains, the number of required simulations
 will be unacceptable.
\end_layout

\begin_layout Standard
In all explained MCMC methods we have calculated the acceptance probability
 
\begin_inset Formula $\alpha=\alpha\left(x,y\right)$
\end_inset

 and we have generated a unity uniform random variable 
\begin_inset Formula $r\sim U\left[0,1\right]$
\end_inset

 in order to accept the candidate 
\begin_inset Formula $y$
\end_inset

 if and only if is lower or equal than acceptance probability
\begin_inset Formula 
\[
x_{t+1}=\begin{cases}
y & \Longleftrightarrow r\leq\alpha\\
x_{t} & \Longleftrightarrow r>\alpha
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Standard
If we force 
\begin_inset Formula $r=1$
\end_inset

 then we would have a random descent method, that only could enhance or
 stay in density.
 In this way, we move from a bayesian simulation method to an optimization
 method, where the puntual convergence will be faster, but it cannot get
 out from local maxima and there are no information about distribution shape.
 If we define a real number 
\begin_inset Formula $r_{0}\in\left[0,1\right]$
\end_inset

 we can build mixtures of MCMC and optimization methods taken 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
r\sim U\left[r_{0},1\right]
\]

\end_inset


\end_layout

\begin_layout Standard
With 
\begin_inset Formula $r_{0}=0$
\end_inset

 it's a pure MCMC method and with 
\begin_inset Formula $r_{0}=1$
\end_inset

 it's a pure optimization method.
 Simulated annealing is an optimization method wich moves from 
\begin_inset Formula $r_{0}=0$
\end_inset

 to 
\begin_inset Formula $r_{0}=1$
\end_inset

 in order to escape of local maxima.
 We don't need to find the global maximum but we can use these mixtures
 methods during the burn-in phase, in order to accelerate puntual convergence
 and be able of escape of flat density regions or complicated constrains.
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
label "Hastings 1970"
key "Hastings 1970"

\end_inset

Hastings, W.K.
 (1970).
 "Monte Carlo Sampling Methods Using Markov Chains and Their Applications".
 Biometrika 57 (1): 97–109.
 doi:10.1093/biomet/57.1.97.
 JSTOR 2334940.
 Zbl 0219.65008.
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
label "Liu-Liang-Wong 2000"
key "Liu-Liang-Wong 2000"

\end_inset

 M.
 J.
 D.
 Powell.
 Recent research at Cambridge on radial basis functions.
 Technical Report DAMTP 1998/NA05, Department of Applied Mathematics and
 Theoretical Physics, University of Cambridge, England, 1998.Liu, J.
 S., Liang, F.
 and Wong, W.
 H.
 (2000).
 The multiple-try method and local optimization in Metropolis sampling,
 Journal of the American Statistical Association, 95(449): 121-134 JSTOR
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
label "Roberts- Rosenthal 2001"
key "Roberts- Rosenthal 2001"

\end_inset

Optimal scaling for various Metropolis-Hastings algorithms, Gareth O.
 Roberts and Jeffrey S.
 Rosenthal, Statistical Science 2001, Vol.
 16, No.
 4, 351–367
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
label "Tjelmeland- Eidsvi 2004"
key "Tjelmeland- Eidsvi 2004"

\end_inset

On the use of local optimizations within Metropolis-Hastings updates, Håkon
 Tjelmeland and Jo Eidsvi, kJournal of the Royal Statistical Society Series
 B, 2004, vol.
 66, issue 2, pages 411-427
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
label "Martino-Read 2013"
key "Martino-Read 2013"

\end_inset

On the flexibility of the design of Multiple Try Metropolis schemes, Luca
 Martino, Jesse Read (arXiv:1201.0646v3)
\end_layout

\end_body
\end_document
