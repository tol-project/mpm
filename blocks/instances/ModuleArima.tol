//////////////////////////////////////////////////////////////////////////////
// FILE    : @ModuleArima.tol
// PURPOSE : ARIMA related modules
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Class @ModuleArima : @ModuleNormalPrior, @GaussianNoise
//////////////////////////////////////////////////////////////////////////////
{
  Real storedInMcmc = 1;
  Real _.hasDensity = True;
  //Set of factors with ARIMA struct
  Set _.factor = Copy(Empty);
  //ARIMA noise
  VMatrix _.z = Constant(0,1,?); 
  //ARMA noise (differenced)
  VMatrix _.w = Constant(0,1,?); 
  //White noise residuals
  VMatrix _.e = Constant(0,1,?); 
  //Conditioning initial values
  VMatrix _.w0 = Constant(0,1,?); 
  VMatrix _.e0 = Constant(0,1,?); 
  //Length of ARMA noise and residuals
  Real _.m=?;
  //Sum of squares of residuals
  Real _.resAvrSqr=?;
  //Sample standard deviation of residuals
  Real _.stdErr = ?;
  //Aggregated difference polynomial
  Polyn  _.dif = 1;
  //Aggregated autoregressive polynomial
  Polyn  _.ar = 1;
  //Aggregated moving-average polynomial
  Polyn  _.ma = 1;
  //Degree of aggregated differences
  Real _.d = 0;
  //Degree of aggregated AR
  Real _.p = 0;
  //Degree of aggregated MA
  Real _.q = 0;
  //Vectorizable information
//Set _arma = Copy(Empty);
  Set _.uncVecStr = Copy(Empty);
  Set _.info = Copy(Empty);
  Set _.eval = Copy(Empty);
  Set _.armaIdx = Copy(Empty);
  Real _.allowUnchangeShortcut = True;
  Real getDataLength(Real void) { _.m };

  Text _.latexName = "\phi,\\theta";
  Text _.latexNoise = "z";
  
  Set _.latexParamName = Copy(Empty); 
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexParamName(Real k) 
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleArima::getLatexParamName] 1 name:"<<_.name);
    If(Card(_.latexParamName), _.latexParamName[k],
    {
      getLatexName(?)+"_{"<<k+"}"
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text getLatexEquation(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text label = "\\texttt{"+Replace(GetLabelFromArima(_.factor),"_","\_")+"}";
"  
\item ARIMA Noise Block "+label+" : "+_.name+" 
\[\n"+
If(_.d,"\\triangle\left(B\right)","")+
If(_.p,"\\phi\left(B\right)","")+_.latexNoise+"="+
If(_.q,"\\theta\left(B\right)","")+"e_{t}"+"
\]
\[
e_{t}\sim N\left(0,\sigma^{2}\right)
\]
"
  };
/*
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexBounds(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    "\n\n\begin{tabular}{ l c r }\n"+ 
    If(!_.p,"","$\left|\\phi\left(z\right)\rigth|$ \\ne 0 \forall |z|\le 1 \\\\\n")+
    If(!_.q,"","$\left|\\theta\left(z\right)\rigth|$ \\ne 0 \forall |z|\le 1 \\\\\n")+
    SetSum( For(1,Card(_paramNames),Text(Real k)
    {
      Real l = VMatDat(_.lowerBound,k,1);
      Real u = VMatDat(_.upperBound,k,1);
      Real L = MPM::StationaryLowerBound(_.maxDegree);
      Real U = MPM::StationaryUpperBound(_.maxDegree);
      If((l<=L) & (u>=U),"",
      {
        "$"+
        If(IsFinite(l),""<<l+"\leq ","")+
        getLatexParamName(k)+
        If(IsFinite(u),"\leq "<<u,"")+
        "$ & : & "<<Replace(_paramNames[k],"_","\_")+" \\\\\n"
      }) +
      If((k%_.dim)&(k>1),
      "\end{tabular}\n"
      "\begin{tabular}{ l c r }\n",
      "")
    }))+
    "\end{tabular}\n\n"
  };  
  */
  ////////////////////////////////////////////////////////////////////////////
  VMatrix getNoise(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _.z
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix getDifNoise(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _.w
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix getResiduals(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real If(!VRows(_.e), logLikelihood(get.x(?)));
    _.e
  };

  ////////////////////////////////////////////////////////////////////////////
  //Evaluates conditioned residuals
  VMatrix eval.residuals.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    getResiduals(?)
  };
  
  //////////////////////////////////////////////////////////////////////////////
  Static Set PolynRootsReport(Polyn pol)
  //////////////////////////////////////////////////////////////////////////////
  {
    Matrix roots = gsl_poly_complex_solve(pol);
    For(1,Rows(roots),Set(Real k)
    {
      Set aux = [[
        Real real = MatDat(roots,k,1);
        Real imaginary = MatDat(roots,k,2);
        Real module = Sqrt(real^2+imaginary^2)
      ]];
      Eval("Set Root_"<<k+"=aux")
    })
  };


  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix Arima2SetOfPolyn(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set s = SetConcat(EvalSet(arima, Set(@ARIMAStruct f)
    {
      If(!Degree(f->AR),Empty,
        SetOfPolyn(ChangeBDegree(f->AR,1/f->Periodicity)))<<
      If(!Degree(f->MA),Empty,
        SetOfPolyn(ChangeBDegree(f->MA,1/f->Periodicity)))
    }))
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Set Arima2SetOfPolynIndex(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set arIdx = SetConcat(For(1,Card(arima), Set(Real k)
    {
      @ARIMAStruct f = arima[k];
      If(!Degree(f->AR),Empty,[[k]])
    }));
    Set maIdx = SetConcat(For(1,Card(arima), Set(Real k)
    {
      @ARIMAStruct f = arima[k];
      If(!Degree(f->MA),Empty,[[k]])
    }));
    [[arIdx,maIdx]]
  };

  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix Arima2Vector(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set s = EvalSet(arima, Matrix(@ARIMAStruct f)
    {
      Matrix ar = Pol2Vec(f->AR, f->Periodicity)
    })<<
    EvalSet(arima, Matrix(@ARIMAStruct f)
    {
      Matrix ma = Pol2Vec(f->MA, f->Periodicity)
    });
    Matrix v = GetNumeric(s);
    Mat2VMat(IfMat(GT(Abs(v),1E-10),v,1E-10),True)
  };

  ////////////////////////////////////////////////////////////////////////////
  Set arima2VectorStruct(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    EvalSet(arima, Matrix(@ARIMAStruct f)
    {
      Matrix ar = Pol2Vec(f->AR, f->Periodicity)
    })<<
    EvalSet(arima, Matrix(@ARIMAStruct f)
    {
      Matrix ma = Pol2Vec(f->MA, f->Periodicity)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix arima2vector(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Matrix v = GetNumeric(arima2VectorStruct(arima));  
    Mat2VMat(IfMat(GT(Abs(v),1E-10),v,1E-10),True)
  };

  ////////////////////////////////////////////////////////////////////////////
  NameBlock vector2arima(VMatrix v)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix vector = IfVMat(GT(Abs(v),1E-10),v,1E-10);
    Set uncVecStr = DeepCopy(_.uncVecStr, VMat2Mat(vector));
    Real F = Card(_.factor);
    Set factor = For(1,F, @ARIMAStruct(Real k)
    {
      @Set f = [[ _.factor[k] ]];
      Polyn AR = Vec2Pol(uncVecStr[k],   $f->Periodicity);
      Polyn MA = Vec2Pol(uncVecStr[k+F], $f->Periodicity);
      @ARIMAStruct($f->Periodicity,AR,MA,$f->DIF )
    });
    [[ 
      vector;
      factor;
      Polyn  ar = ARIMAGetAR(factor);
      Polyn  ma = ARIMAGetMA(factor);
      Real ar.stationary = {
         Real is = IsStationary(ar);
       //If(!is, WriteLn("TRACE Non stationary AR "<<ar));
         is
      };
      Real ma.stationary =  {
         Real is = IsStationary(ma);
       //If(!is, WriteLn("TRACE Non stationary MA "<<ma));
         is
      }
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real _setup.arma(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleArima::setup.arma] address:"<<getMID(?));
    Polyn  _.dif := ARIMAGetDIF(_.factor);
    Polyn  _.ar := ARIMAGetAR(_.factor);
    Polyn  _.ma := ARIMAGetMA(_.factor);
    Real _.d := Degree(_.dif);
    Real _.p := Degree(_.ar);
    Real _.q := Degree(_.ma);
    VMatrix If(!VRows(_.w0), _.w0 := Constant(_.p,1,0) );
    VMatrix If(!VRows(_.e0), _.e0 := Constant(_.q,1,0) );
    VMatrix _mrv := arima2vector(_.factor);
    Real _.n := VRows(_mrv);
    Real calc.difNoise(?);  
    Real _.m := VRows(_.w);
    Set _.uncVecStr := DeepCopy(arima2VectorStruct(_.factor));
    Set _.armaIdx := @ModuleArima::Arima2SetOfPolynIndex(_.factor);
        
    VMatrix  If(!VRows(_.lowerBound),
      _.lowerBound := Constant(_.n,1,-1/0));
    VMatrix  If(!VRows(_.upperBound),
      _.upperBound := Constant(_.n,1,+1/0));
    VMatrix _.lowerBound := IfVMat(IsUnknown(_.lowerBound),-1/0,_.lowerBound);
    VMatrix _.upperBound := IfVMat(IsUnknown(_.upperBound),+1/0,_.upperBound);
    VMatrix lowerBound.ar = Constant(0,1,?);
    VMatrix upperBound.ar = Constant(0,1,?);
    VMatrix lowerBound.ma = Constant(0,1,?);
    VMatrix upperBound.ma = Constant(0,1,?);
    Set For(1,Card(_.factor),Real(Real k)
    {
      @ARIMAStruct fk = _.factor[k];
      Real p = Degree(fk->AR)/fk->Periodicity;
      Real q = Degree(fk->MA)/fk->Periodicity;
      VMatrix lowerBound.ar := lowerBound.ar << MPM::StationaryLowerBound(p);
      VMatrix upperBound.ar := upperBound.ar << MPM::StationaryUpperBound(p);
      VMatrix lowerBound.ma := lowerBound.ma << MPM::StationaryLowerBound(q);
      VMatrix upperBound.ma := upperBound.ma << MPM::StationaryUpperBound(q);
      True
    });
    VMatrix _.lowerBound := Max(_.lowerBound, 
      lowerBound.ar << lowerBound.ma);
    VMatrix _.upperBound := Min(_.upperBound,
      upperBound.ar << upperBound.ma);
    Set If(Card(_paramNames)!=_.n, _paramNames := 
    SetConcat(For(1,Card(_.factor),Set(Real k)
    {
      @ARIMAStruct fk = _.factor[k];
      Text prefix = _.name+".Factor."<<k+"_";
      EvalSet(Monomes(1-fk->AR),Text(Polyn mon)
      {
        prefix+"AR."<<Degree(mon)
      })
    }))<<
    SetConcat(For(1,Card(_.factor),Set(Real k)
    {
      @ARIMAStruct fk = _.factor[k];
      Text prefix = _.name+".Factor."<<k+"_";
      EvalSet(Monomes(1-fk->MA),Text(Polyn mon)
      {
        prefix+"MA."<<Degree(mon)
      })
    })));
    Set If(Card(_.latexParamName)!=_.n, _.latexParamName := 
    SetConcat(For(1,Card(_.factor),Set(Real k)
    {
      @ARIMAStruct fk = _.factor[k];
      EvalSet(Monomes(1-fk->AR),Text(Polyn mon)
      {
        "\phi^{k}_{"<<Degree(mon)+"}"
      })
    }))<<
    SetConcat(For(1,Card(_.factor),Set(Real k)
    {
      @ARIMAStruct fk = _.factor[k];
      EvalSet(Monomes(1-fk->MA),Text(Polyn mon)
      {
        "\\theta^{k}_{"<<Degree(mon)+"}"
      })
    })));
    Real setParamNames(_paramNames);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real setup(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleArima::setup] address:"<<getMID(?));
    _setup.arma(?);
    check.param(?);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Initializes the Markov chain to automatic generated values
  Real setInitialValues.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleArima::setInitialValues.auto] address:"<<getMID(?));
    Real F = Card(_.factor);
  //NameBlock smp = defSampler(?);
  //Real smp::set.covarianze(Eye(_.n));
  //Real smp::tunning.Mode := BysSampler::@RandWalk::TunningMode::Diagonal;
  //Real smp::tunning.Mode := BysSampler::@RandWalk::TunningMode::Full;
    Set For(1,F, Real(Real k)
    {
      @ARIMAStruct fk = _.factor[k];
      Real s = fk->Periodicity;
      Polyn fk->AR := RandStationary(Degree(fk->AR)/s,s);
      Polyn fk->MA := RandStationary(Degree(fk->MA)/s,s);
      True
    });
    Real ok = set.x(arima2vector(_.factor));
    ok
  };
/*
  ARIMAAlmagroEval {Set ARIMAAlmagroEval(Set arima, Matrix output [, Real sigma=1.0])} C:/users/toldevel/trunk/tol/btol/bmodel/estim.cpp {Calcula los residuos del modelo con valores iniciales máximo verosímiles usando el algoritmo de Almagro.
NOTA: Si la matriz de output ya está diferenciada hay que introduciren arima el polinomio de diferencias 1 en todas las estacionalidades.
Si todo funciona correctamente devuelve lo siguiente:
Matrix z:Ruido
Matrix w:Ruido diferenciado
Matrix a:Tra(Psi)*Inv(ACOV)*W : Residuos del modelo condicionados al ruido diferenciado.
Matrix ACOV:Función de autocoavarianzas del modelo
Matrix ACF:Función de Autocorrelaciones del modelo
Real ACOVDetN:Det(ACOV)^(1/N): Raíz N-ésima del determinante de la matriz de autocoavarianzas
Real wtACOViw:w'*ACOViw: Suma de cuadrados del ruido diferenciado en la métrica de las autocovarianzas.
Real aS2:a'*a : Suma de cuadrados de los residuos
Real logLH:-(1/2)*(N*Log(2*pi*sigma^2) + N*Log(ACOVDetN) + wtACOViw/sigma^2) : Logaritmo de la función de verosimilitud
Matrix w0:Los p valores iniciales del ruido diferenciado. Sólo los devuelve si calcInitialValues es cierto
Matrix a0:Los q valores iniciales del ruido diferenciado. Sólo los devuelve si calcInitialValues es cierto
}
*/
  ////////////////////////////////////////////////////////////////////////////
  //Calculates exact ARMA log-density
  NameBlock Evaluate_Almagro(NameBlock info, Real calcInitVal)
  ////////////////////////////////////////////////////////////////////////////
  {[[
    Set eval = ARIMAAlmagroEval(info::factor,VMat2Mat(_.z),1);
    VMatrix w = _.w;
    VMatrix e = Mat2VMat(eval::a);
    VMatrix w0 = If(!calcInitVal,_.w0,Mat2VMat(eval::w0));
    VMatrix e0 = If(!calcInitVal,_.e0,Mat2VMat(eval::a0));
    Real sigma = _.sigma;
    Real resAvrSqr = eval::aS2/_.m;
    Real logDens = 
       _.m*@GaussianNoise::_.cte.1 
      -_.m*Log(_.sigma)
      -0.5*Log(eval::ACOVDetN*_.m) 
      -0.5*eval::wtACOViw/_.sigma^2
  ]]};


  ////////////////////////////////////////////////////////////////////////////
  //Calculates exact ARMA log-density
  NameBlock Evaluate_Levinson(NameBlock info, Real calcInitVal)
  ////////////////////////////////////////////////////////////////////////////
  {[[
    Set eval = ARIMALevinsonEval(info::factor,VMat2Mat(_.z),calcInitVal);
    VMatrix w = _.w;
    VMatrix e = Mat2VMat(eval::a);
    VMatrix w0 = If(!calcInitVal,_.w0,Mat2VMat(eval::w0));
    VMatrix e0 = If(!calcInitVal,_.e0,Mat2VMat(eval::a0));
    Real sigma = _.sigma;
    Real resAvrSqr = eval::aS2/_.m;
    Real logDens = 
       _.m*@GaussianNoise::_.cte.1 
      -_.m*Log(_.sigma)
      -0.5*Log(eval::ACOVDetN*_.m) 
      -0.5*eval::wtACOViw/_.sigma^2
  ]]};

  ////////////////////////////////////////////////////////////////////////////
  //Calculates truncated approximation to exac ARMA log-density 
  NameBlock Evaluate_TruncLevinson(NameBlock info, Real calcInitVal)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set arma = EvalSet(info::factor,Set(@ARIMAStruct f)
    {
      @ARIMAStruct(f->Periodicity,f->AR,f->MA,1)
    });
    Real m.1 = Round(Min(Max($_config::arima.TruncLevinsonRatio*(1+_.p+_.q),30),_.m));
    Matrix w.1 = VMat2Mat(Sub(_.w,1,1,m.1,1));
    Matrix w.2 = VMat2Mat(Sub(_.w,m.1+1,1,_.m-m.1,1));
    Real old.v = Show(False,"ALL");
    Set eval = ARIMALevinsonEval(arma,w.1,calcInitVal);
    Real Show(old.v,"ALL");
    [[
      Set eval;
      VMatrix w = _.w;
      VMatrix e = Mat2VMat(eval::a << DifEq(info::ar/info::ma,w.2,w.1,eval::a));
      VMatrix w0 = If(!calcInitVal,_.w0,Mat2VMat(eval::w0));
      VMatrix e0 = If(!calcInitVal,_.e0,Mat2VMat(eval::a0));
      Real sigma = _.sigma;
      Real resAvrSqr = VMatAvr(e^2);
      Real logDens = _.m*(
         @GaussianNoise::_.cte.1 
        -Log(_.sigma)
        -0.5*Log(eval::ACOVDetN*_.m)/_.m
        -0.5*resAvrSqr/(_.sigma^2))
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  //Calculates ARMA log-density conditioned to initial values
  NameBlock Evaluate_CondInitVal_AR_MA(Polyn ar, Polyn ma)
  ////////////////////////////////////////////////////////////////////////////
  {[[
    VMatrix w = _.w;
    VMatrix e = DifEq(ar/ma, _.w, _.w0, _.e0);
    VMatrix w0 = _.w0;
    VMatrix e0 = _.e0;
    Real sigma = _.sigma;
    Real resAvrSqr = VMatAvr(e^2);
    Real logDens = _.m*(
      @GaussianNoise::_.cte.1 - Log(_.sigma) - 0.5*resAvrSqr/(_.sigma^2))
  ]]};

  Real _.unchangedARMAEval = 0;
  Real _.changedARMAEval = 0;


  ////////////////////////////////////////////////////////////////////////////
  Real IsUnchangedARMA(NameBlock info)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real unchangedARMA = _.allowUnchangeShortcut*
      If(Or(!Card(_.info),!Card(_.eval)), False,
      VMatSum(NE(info::vector,$_.info::vector))==0);
    Real _.unchangedARMAEval := _.unchangedARMAEval + unchangedARMA;
    Real _.changedARMAEval := _.changedARMAEval + Not(unchangedARMA);
    unchangedARMA    
  };

  ////////////////////////////////////////////////////////////////////////////
  //Calculates truncated approximation to exac ARMA log-density 
  NameBlock Evaluate_FastChol(NameBlock info, Real calcInitVal)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [Evaluate_FastChol] ");
    Real unchangedARMA = IsUnchangedARMA(info);
    Real hasFastChol = ObjectExist("Set","_.eval::fastChol");
    [[
      @NameBlock fastChol = If(unchangedARMA & hasFastChol, _.eval::fastChol,
        [[ArimaTools::ARMAProcess::@FastCholeskiCovFactor:: 
          Create(info::ar, info::ma, _.m)]]);  
      VMatrix w = _.w;
      VMatrix e = $fastChol::filter(_.w);
      VMatrix w0 = _.w0;
      VMatrix e0 = _.e0;
      Real sigma = _.sigma;
      Real resAvrSqr = If(Not($fastChol::_.isGood),1/0,VMatAvr(e^2));
      Real logDens = If(Not($fastChol::_.isGood),-1/0,
        -_.m*0.5*Log(2*PI)
        -_.m*Log(_.sigma) 
        -0.5*$fastChol::_.logDetCov
        -_.m*0.5*resAvrSqr/(_.sigma^2))
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  //Calculates truncated approximation to exac ARMA log-density 
  NameBlock Evaluate_FastCholSea(NameBlock info, Real calcInitVal)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [Evaluate_FastCholSea] ");
    Real noPreviousARMA = Or(!Card(_.info),!Card(_.eval));
    Real unchangedARMA = IsUnchangedARMA(info);
    VMatrix e = _.w;
    Real logDetCov = 0; 
    Real isGood = True;
    Real k = 1;
    Real K = Card(info::factor);
    Set fastCholSea = Copy(Empty);
    While(isGood & (k<=K),
    {
      Real unchangedARMAFactor = Case(
        unchangedARMA, True, 
        noPreviousARMA, False,
        Card($_.eval::fastCholSea)<k, False,  
        info::factor[k]->AR != $_.info::factor[k]->AR, False,
        info::factor[k]->MA != $_.info::factor[k]->MA, False,
        1==1, True);
      @NameBlock fastChol = If(unchangedARMAFactor, $_.eval::fastCholSea[k],
        [[ArimaTools::ARMAProcess::@FastCholeskiCovFactor::
          Create(info::factor[k]->AR, info::factor[k]->MA, _.m)]]);
      VMatrix e := $fastChol::filter(e);
      Real isGood := $fastChol::_.isGood; 
      Real logDetCov := logDetCov + $fastChol::_.logDetCov; 
      Set Append(fastCholSea,fastChol);
      Real k:=k+1
    });
    [[
      Set fastCholSea;
      VMatrix w = _.w;
      VMatrix e;
      VMatrix w0 = _.w0;
      VMatrix e0 = _.e0;
      Real sigma = _.sigma;
      Real resAvrSqr = If(Not(isGood),1/0,VMatAvr(e^2));
      Real logDens = If(Not(isGood),-1/0,
        -_.m*0.5*Log(2*PI)
        -_.m*Log(_.sigma) 
        -0.5*logDetCov
       -_.m*0.5*resAvrSqr/(_.sigma^2))
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  //Calculates ARMA log-density conditioned to initial values
  NameBlock Evaluate_CondInitVal(NameBlock info, Real calcInitVal)
  ////////////////////////////////////////////////////////////////////////////
  {
    Evaluate_CondInitVal_AR_MA(info::ar, info::ma)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Calculates ARMA log-density by configurated method
  NameBlock Evaluate(NameBlock info, Real calcInitVal)
  ////////////////////////////////////////////////////////////////////////////
  {
    Eval("Evaluate_"+$_config::arima.EvalMethod+"(info, calcInitVal)")    
  };
    
  ////////////////////////////////////////////////////////////////////////////
  //Calculates ARMA log-density by configurated method
  NameBlock Evaluate.auto(Real calcInitVal)
  ////////////////////////////////////////////////////////////////////////////
  {
    Evaluate(vector2arima(_mrv),calcInitVal)
  };
    
  ////////////////////////////////////////////////////////////////////////////
  //Calculates ARMA log-density by configurated method
  NameBlock vector2arima.auto(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    vector2arima(_mrv)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real calc.difNoise(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.w := Sub(DifEq(_.dif/1,_.z),_.d+1,1,VRows(_.z)-_.d,1);
  //WriteLn("TRACE [@ModuleArima::calc.difNoise] StdErr(w)="<<Sqrt(VMatAvr(_.w^2)));
  //WriteLn("TRACE [@ModuleArima::calc.difNoise] w="<<VMat2Mat(_.w,1));
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real setNoise(VMatrix z) 
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.z := z;
  //WriteLn("TRACE [@ModuleArima::setNoise] z="<<VMat2Mat(_.z,1));
    calc.difNoise(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real eval.noise(VMatrix z) 
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix old.z = _.z;
    Real setNoise(z);
    NameBlock ev = Evaluate_CondInitVal_AR_MA(_.ar, _.ma);
    Real setNoise(old.z);
    ev::logDens
  };

  ////////////////////////////////////////////////////////////////////////////
  Real getNumIneq(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _getNumIneq.ModuleParam(?) + Card(_.armaIdx[1])+Card(_.armaIdx[2])
  };
  
    ////////////////////////////////////////////////////////////////////////////
  VMatrix inequality_constraint.current(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    NameBlock info = vector2arima(_mrv);
    Set armaTr = Traspose(info::factor);
    Set ar = ExtractByIndex(armaTr[2],_.armaIdx[1]);
    Set ma = ExtractByIndex(armaTr[3],_.armaIdx[2]);
    _inequality_constraint_moduleParam(_mrv) <<
    Mat2VMat(SetCol(EvalSet(ar<<ma,Real(Polyn pol)
    {
      -Log(StationaryValue(pol))
    })))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  VMatrix inequality_constraint(VMatrix z) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real set.x(z);
    inequality_constraint.current(?)
  };


  ////////////////////////////////////////////////////////////////////////////
  Set getIneqStatus(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    NameBlock info = vector2arima(_mrv);
    Real j = 0;
    _getIneqStatus.Module(?)<<
    SetConcat(For(1,Card(info::factor),Set(Real k)
    {
      @ARIMAStruct fk = info::factor[k];
      Text prefix = _.name+"_Factor."<<k+"_Period."<<fk->Periodicity+"_";
      [[
        { [[
          Text CombBoundId = prefix+"AR.Stationarity";
          Real CombEvalLE0 = -Log(StationaryValue(fk->AR));
          Real Matches = CombEvalLE0<=0
        ]] },
        { [[
          Text CombBoundId = prefix+"MA.Stationarity";
          Real CombEvalLE0 = -Log(StationaryValue(fk->MA));
          Real Matches = CombEvalLE0<=0
        ]] }
      ]]
    }))
  };


  ////////////////////////////////////////////////////////////////////////////
  //Returns ARMA log-density conditioned to initial values
  Real logLikelihood(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    set.x(z);
    logLikelihood.current(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Returns ARMA log-density conditioned to initial values
  Real logLikelihood.current(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    NameBlock info = vector2arima(_mrv);
  //WriteLn("TRACE [logLikelihood.current] _mrv="<<VMat2Mat(_mrv,1));
    Set _.factor := info::factor;
    Polyn  _.ar := info::ar;
    Polyn  _.ma := info::ma;     
    Set _.eval := @NameBlock(Evaluate(info, True));  
    Set _.info := @NameBlock(info);
    VMatrix _.e := $_.eval::e;
  //WriteLn("TRACE [@ModuleArima::logLikelihood.current] e="<<VMat2Mat(_.e,1));
    VMatrix _.w0 := $_.eval::w0;
    VMatrix _.e0 := $_.eval::e0;
    Real _.resAvrSqr := $_.eval::resAvrSqr;
    Real _.stdErr := Sqrt(_.resAvrSqr);
    Real llh = $_.eval::logDens;
  //If(IsUnknown(llh),-1/0,llh)
    llh
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _clean.Arima (Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set _.uncVecStr := Copy(Empty);
    Set _.info := Copy(Empty);
    Set _.eval := Copy(Empty);
    Set _.armaIdx := Copy(Empty);
    True 
  };

  ////////////////////////////////////////////////////////////////////////////
  Real clean (Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleArima::_clean] ");
    _clean.Arima(?);
    _clean.Module (?)
  }

};


