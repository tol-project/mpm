 //////////////////////////////////////////////////////////////////////////////
// FILE    : Module.tol
// PURPOSE : Implementation of Class MPM::@Module
//////////////////////////////////////////////////////////////////////////////

Class @PriorLinComb;
Class @Relation;

//////////////////////////////////////////////////////////////////////////////
Class @Module : @Engine, BysSampler::@Target
//////////////////////////////////////////////////////////////////////////////
{
  Text _.name = ""; 
  Real _.instanceIndex = ?;
  Real storedInMcmc;
  Real _.hasDensity;
  Set _conditionedRelations = Copy(Empty);
  Set _conditionedModules = Copy(Empty);
  Set _conditionedRelationsCascade = Copy(Empty);
  Set _conditionedModulesCascade = Copy(Empty);
  @NameBlock _config = @NameBlock(MPM::@Config::Default(?));
  Real _.n = 0;

  Real getNumParam(Real void) { _.n };
  Real getPriorNumber(Real void) { 0 };
  
  VMatrix getLowerBound(Real void);
  VMatrix getUpperBound(Real void);
  Real getNumIneq(Real void);
  Set getCombConstraints(Real void);

  Text getParamName(Real j) { "" }; 
  Set  getParamNames(Real void) { Copy(Empty) };
  Set  getParamNames.transf(Real void) { Copy(Empty) };
  Real findParam(Text paramName);
  Real getParamValue(Text paramName);
  Real setParamValue(Text paramName, Real value);
  Real SetDerivativeMode(Real dm);
  Real setParamNames(Set paramNames);
  Real check.size(Real void);
  Real check.bounds(Real void);
  Real check.param(Real void);
  Real setup(Real void);
  Real setInitialValues.auto(Real void);
  VMatrix get.x.transf(Real void) { get.x(?) };
  VMatrix get.x(Real void);
  VMatrix _.get.x(Real void);
  Real set.x(VMatrix x);
  Real sum.x(VMatrix x);
  Real setScalar(Real x);
  Real match_bounds(VMatrix z);
  VMatrix bounds_constraint(VMatrix z);
  VMatrix bounds_constraint.current(Real void);
  VMatrix inequality_constraint(VMatrix z);
  VMatrix inequality_constraint.current(Real void);
  Real is_feasible.current(Real void);
  Real pi_logDens(VMatrix z);
  Real logLikelihood(VMatrix z);
  Real logPrior     (VMatrix z);
  Real match_bounds.current (Real void);
  Real logLikelihood.current(Real void);
  Real logPrior.current     (Real void);
  Real pi_logDens.current   (Real void);
  Real pi_logDens_M(Matrix y);
  Set getParamStatus(Real void);
  Real addCombBounds(
    Text name, 
    Set parameters, Set coefficients,
    Real lower, Real upper);
  Real updateSampler(Real void) { True };

  Text _.latexName = "";
  Real _.latexFilterReverseOrder = False;
  
  Text getLatexAppend(Real void) { "" };
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexName(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Case(
      _.latexName=="",_.name, 
      _.latexName==" ","",
      1==1, _.latexName)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexParamName(Real k) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@Module::getLatexParamName] 1 name:"<<_.name);
    getLatexName(?)+"_{"<<k+"}"
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexEquation(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    "Not implemented LATEX for module "+ClassOf(_this)+" "+_.name 
  };

  ////////////////////////////////////////////////////////////////////////////
  Text getLatexBounds(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    ""
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexIneq(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    ""
  };
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexPrior(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    ""
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Static @Module New(
    Text className, 
    Text name,
    @Config cfg)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text expr = className+" "+name+" = [[\n"+
    "  Text _.name = name;\n"+
    "  @NameBlock _config = @NameBlock(cfg) ]];\n";
  //WriteLn("TRACE expr="<<expr);
    Eval(expr)
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @NameBlock NewRef(
    Text className, 
    Text name,
    @Config cfg)
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock(@Module::New(className,name,cfg))
  };

  ////////////////////////////////////////////////////////////////////////////
  Real addConditionedRelation(@Relation m)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set Append(_conditionedRelations,[[m]]);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real addConditionedModule(@Module m)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real find = FindIndexByName(_conditionedModules,m::_.name);
    Set If(!find, Append(_conditionedModules,[[m]],True));
    True
  };


  ////////////////////////////////////////////////////////////////////////////
  Real _.buildListOfConditionedModules(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Module::_buildListOfConditionedModules] 1 "+_.name+" list:"<<Card(_conditionedModulesCascade));
    Real found = FindIndexByName(_conditionedModulesCascade, _.name);
  //WriteLn("TRACE [@Module::_buildListOfConditionedModules] 2 found:"<<found);
    If(found,False,
    {
    //WriteLn("TRACE [@Module::_buildListOfConditionedModules] 3 "+_.name+" list:"<<Card(_conditionedModulesCascade));
    //WriteLn("TRACE [@Module::_buildListOfConditionedModules] 4 _conditionedModules:"<<Card(_conditionedModules));
      Set EvalSet(_conditionedModules,Real(@Module m)
      {
      //WriteLn("TRACE [@Module::_buildListOfConditionedModules] 5 "+m::_.name);
        Real found = FindIndexByName(_conditionedModulesCascade, m::_.name);
      //WriteLn("TRACE [@Module::_buildListOfConditionedModules] 6 found:"<<found);
        If(found, False,
        {
          Set Append(_conditionedModulesCascade, [[m]], True);
          Set Append(_conditionedModulesCascade, m::getConditionedModulesCascade(False),True);
          True 
        })
      });
      True
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Set getConditionedModulesCascade(Real force)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Module::getConditionedModulesCascade] 1 force:"<<force);
    Real If(force | !Card(_conditionedModulesCascade),
    {
    //WriteLn("TRACE [@Module::getConditionedModulesCascade] 2 ");
      _.buildListOfConditionedModules(?)
    });
    _conditionedModulesCascade
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _.buildListOfConditionedRelations(Set list, Set visitedModules)
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock T = [[_this]];
  //WriteLn("TRACE [@Module::buildListOfConditionedRelations] 1 ");
    Set Append(visitedModules, T, True);
    Set EvalSet(_conditionedRelations,Real(@Relation r)
    {
      Set Append(list,[[r]],True);
      True
    });
    Set EvalSet(_conditionedModules,Real(@Module m)
    {
      Real found = FindIndexByName(visitedModules, m::_.name);
      If(found, False,
      {
        m::_.buildListOfConditionedRelations(list, visitedModules)
      })
    });
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Set getConditionedRelationsCascade(Real force)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Module::getConditionedRelationsCascade] 1 force:"<<force);
    Real If(force | !Card(_conditionedRelationsCascade),
    {
    //WriteLn("TRACE [@Module::getConditionedRelationsCascade] 2 ");
      Set vistedModules = Copy(Empty);
      _.buildListOfConditionedRelations(
        _conditionedRelationsCascade, vistedModules);
      Set _conditionedRelationsCascade := Sort(_conditionedRelationsCascade,
      Real (@Relation a, @Relation b)
      {
        Compare(a::_.precedence, b::_.precedence)
      });
      True
   });
    _conditionedRelationsCascade
  };


  ////////////////////////////////////////////////////////////////////////////
  MPM::@Config getConfig(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    $_config
  };

  ////////////////////////////////////////////////////////////////////////////
  @NameBlock getConfigRef(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _config
  };

  ////////////////////////////////////////////////////////////////////////////
  Real setConfig(MPM::@Config cfg)
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock _config := @NameBlock(cfg);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real setConfigParam(Text name, Anything value)
  ////////////////////////////////////////////////////////////////////////////
  {
    Anything _config[name] := value;
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Text getMID(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock T = [[_this]];
    FullName($T)
  };

  ////////////////////////////////////////////////////////////////////////////
  Set getModuleStatus(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [getModuleStatus] 1");
    Text ClassName = ClassOf(_this);
  //WriteLn("TRACE [getModuleStatus] 2");
    Text Module = _.name;
  //WriteLn("TRACE [getModuleStatus] 3");
    Real NumParamFree = VMatSum(LT(getLowerBound(?),getUpperBound(?)));
  //WriteLn("TRACE [getModuleStatus] 4");
    Real NumData = getDataLength(?);
  //WriteLn("TRACE [getModuleStatus] 5");
    Real NumPrior = getPriorNumber(?);
  //WriteLn("TRACE [getModuleStatus] 6");
    Real NumParam = getNumParam(?);
  //WriteLn("TRACE [getModuleStatus] 7");
    Real NumParamFixed = VMatSum(EQ(getLowerBound(?),getUpperBound(?)));
  //WriteLn("TRACE [getModuleStatus] 8");
    Real StoredInMcmc = storedInMcmc;
  //WriteLn("TRACE [getModuleStatus] 9");
    Real HasDensity = _.hasDensity;
  //WriteLn("TRACE [getModuleStatus] 10");
    Real NumLowerBounds = VMatSum(IsFinite(getLowerBound(?)));
  //WriteLn("TRACE [getModuleStatus] 11");
    Real NotMatchingLowerBounds = VMatSum(GT(getLowerBound(?),get.x(?)));
  //WriteLn("TRACE [getModuleStatus] 12");
    Real NumUpperBounds = VMatSum(IsFinite(getUpperBound(?)));
  //WriteLn("TRACE [getModuleStatus] 13");
    Real NotMatchingUpperBounds = VMatSum(LT(getUpperBound(?),get.x(?)));
  //WriteLn("TRACE [getModuleStatus] 14");
    Real NumInequations = getNumIneq(?);
  //WriteLn("TRACE [getModuleStatus] 15");
    Real NotMatchingInequations = {
      Set ies = getIneqStatus(?);
      If(!Card(ies),0, SetSum(EvalSet(ies,Real(Set s) { Not(s::Matches) }))) };
  //WriteLn("TRACE [getModuleStatus] 16");
    Real IsFeasible = is_feasible.current(?);
  //WriteLn("TRACE [getModuleStatus] 17");
    Real LogPrior = logPrior.current(?);
  //WriteLn("TRACE [getModuleStatus] 18");
    Real LogLikelihood = logLikelihood.current(?);
  //WriteLn("TRACE [getModuleStatus] 19");
    Real LogPosteriorDens = If(IsFeasible,LogPrior+LogLikelihood,-1/0);
  //WriteLn("TRACE [getModuleStatus] 20");
  [[ [[
    Text ClassName,
    Text Module,
    Real NumParamFree,
    Real NumData,
    Real NumPrior,
    Real NumParam,
    Real NumParamFixed,
    Real StoredInMcmc,
    Real HasDensity,
    Real NumLowerBounds,
    Real NotMatchingLowerBounds,
    Real NumUpperBounds,
    Real NotMatchingUpperBounds,
    Real NumInequations,
    Real NotMatchingInequations,
    Real IsFeasible,
    Real LogPrior,
    Real LogLikelihood,
    Real LogPosteriorDens
  ]] ]]};

  ////////////////////////////////////////////////////////////////////////////
  Set _getIneqStatus.Module(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Module::_getIneqStatus.Module] ");
    Set cc = getCombConstraints(?);
    If(Or(!getNumIneq(?),!Card(cc)), Copy(Empty),
      $cc::getIneqStatus(get.x(?)))
  };

  ////////////////////////////////////////////////////////////////////////////
  Set getIneqStatus(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Module::getIneqStatus] ");
    _getIneqStatus.Module(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _clean.Module (Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set _config := Copy(Empty);
    Set _conditionedRelations := Copy(Empty);
    Set _conditionedModules := Copy(Empty);
    Set _conditionedRelationsCascade := Copy(Empty);
    Set _conditionedModulesCascade := Copy(Empty);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real clean (Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _clean.Module (?)
  }; 

  ////////////////////////////////////////////////////////////////////////////
  Real __destroy (Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    clean(?)
  }

};

#Embed "ModuleParam.tol";
#Embed "ModuleNoParam.tol";
#Embed "randVars/Primary.tol";
#Embed "randVars/Oper.Scalar.tol";
#Embed "priors/PriorLinComb.tol";
#Embed "priors/PriorScalarTruncNormal.tol";
#Embed "embedding/ModuleEmbedding.tol";
#Embed "instances/ModuleNormalPrior.tol";
#Embed "instances/ModuleChangeOfVariable.tol";
#Embed "instances/ModuleFilter.tol";
#Embed "instances/ModuleLinear.tol";
#Embed "instances/ModuleNonLinearFilter.tol";
#Embed "instances/ModuleBass.tol";
#Embed "instances/ModuleTransferFunc.tol";
#Embed "instances/ModulePiecewise.tol";
#Embed "instances/ModuleMissing.tol";
#Embed "instances/ModuleGarch.tol";
#Embed "instances/ModuleArima.tol";
#Embed "instances/ModuleGaussianVariance.tol";
#Embed "instances/ModuleExternalParam.tol";
#Embed "instances/ModuleExternalLinearComb.tol";
#Embed "instances/ModuleExternNormalPrior.tol";
#Embed "instances/ModuleLatentLinear.tol";
#Embed "instances/ModuleGaussianDiagonalCovariance.tol";
#Embed "instances/ModuleGaussianCorrelation.tol";
#Embed "instances/ModuleGaussianCorrelation.ChVr.tol";
#Embed "instances/ModuleGaussianCovariance.tol";
#Embed "instances/ModuleVAR.tol";
#Embed "instances/ModuleGeneralizedRegression.tol";

